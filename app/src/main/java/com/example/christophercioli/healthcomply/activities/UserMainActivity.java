// Christopher Cioli
// P&P6 - 1807
// UserMainActivity.java

package com.example.christophercioli.healthcomply.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.christophercioli.healthcomply.R;
import com.example.christophercioli.healthcomply.contracts.DatabaseContracts;
import com.example.christophercioli.healthcomply.fragments.AdminMyOrganizationFragment;
import com.example.christophercioli.healthcomply.fragments.AdminRosterFragment;
import com.example.christophercioli.healthcomply.fragments.AdministratorCredentialsFragment;
import com.example.christophercioli.healthcomply.fragments.ProOrganizationsFragment;
import com.example.christophercioli.healthcomply.fragments.ProfessionalCredentialsFragment;
import com.example.christophercioli.healthcomply.fragments.ProfileFragment;
import com.example.christophercioli.healthcomply.models.Administrator;
import com.example.christophercioli.healthcomply.models.Organization;
import com.example.christophercioli.healthcomply.models.Professional;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class UserMainActivity extends AppCompatActivity {

    // UI Elements
    private BottomNavigationView mBottomNavigationView;

    // Constants
    public static final String FRAGMENT_FUNCTION_KEY = "FRAGMENT_FUNCTION_KEY";
    public static final String TO_PROFESSIONAL_CREDENTIALS = "TO_PROFESSIONAL_CREDENTIALS";
    public static final String TO_ADMINISTRATOR_CREDENTIALS = "TO_ADMINISTRATOR_CREDENTIALS";
    public static final String EXTRA_PROFESSIONAL_USER = "EXTRA_PROFESSIONAL_USER";
    public static final String EXTRA_ADMINISTRATOR_USER = "EXTRA_ADMINISTRATOR_USER";
    public static final String SECTION_PRO_CREDENTIALS = "PRO_CREDENTIALS";
    public static final String SECTION_PRO_ORGANIZATIONS = "PRO_ORGANIZATIONS";
    public static final String SECTION_PRO_PROFILE = "PRO_PROFILE";
    public static final String SECTION_ADMIN_CREDENTIALS = "ADMIN_CREDENTIALS";
    public static final String SECTION_ADMIN_ORGANIZATION = "ADMIN_ORGANIZATION";
    public static final String SECTION_ADMIN_ROSTER = "ADMIN_ROSTER";
    public static final String SECTION_ADMIN_PROFILE = "ADMIN_PROFILE";

    // Member Variables
    private DatabaseReference mDatabase;
    private Professional mProfessional;
    private Administrator mAdministrator;
    public String mCurrentSection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);

        mBottomNavigationView = findViewById(R.id.bottomNavUserMain);

        Intent inboundIntent = getIntent();
        String fragmentFunction = inboundIntent.getStringExtra(FRAGMENT_FUNCTION_KEY);

        setUpFragment(inboundIntent, fragmentFunction);
    }

    // Activity Functionality
    private void setUpFragment(Intent _inboundIntent, String _fragmentFunction) {
        mDatabase = FirebaseDatabase.getInstance().getReference();

        switch(_fragmentFunction) {
            case TO_PROFESSIONAL_CREDENTIALS:
                mCurrentSection = SECTION_PRO_CREDENTIALS;
                mBottomNavigationView.inflateMenu(R.menu.pro_bottom_nav);
                setMenuOnClickListeners(false);
                mProfessional = (Professional) _inboundIntent
                        .getSerializableExtra(EXTRA_PROFESSIONAL_USER);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frameLayoutUserMain,
                        ProfessionalCredentialsFragment.newInstance(mProfessional))
                        .addToBackStack(null).commit();
                break;
            case TO_ADMINISTRATOR_CREDENTIALS:
                mCurrentSection = SECTION_ADMIN_CREDENTIALS;
                mBottomNavigationView.inflateMenu(R.menu.admin_bottom_nav);
                setMenuOnClickListeners(true);
                mAdministrator = (Administrator) _inboundIntent
                        .getSerializableExtra(EXTRA_ADMINISTRATOR_USER);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frameLayoutUserMain,
                                AdministratorCredentialsFragment.newInstance(mAdministrator))
                        .addToBackStack(null).commit();

        }
    }

    // Bottom Nav Functionality
    private void setMenuOnClickListeners(boolean _isAdmin) {
        if (!_isAdmin) {
            mBottomNavigationView.setOnNavigationItemSelectedListener(
                    new BottomNavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.buttonProNavCredentials:
                                    if (!mCurrentSection.equals(SECTION_PRO_CREDENTIALS)) {
                                        mCurrentSection = SECTION_PRO_CREDENTIALS;
                                        getSupportFragmentManager().beginTransaction()
                                                .replace(R.id.frameLayoutUserMain,
                                                        ProfessionalCredentialsFragment
                                                                .newInstance(mProfessional))
                                                .addToBackStack(null).commit();
                                    }
                                    return true;
                                case R.id.buttonProNavOrganizations:
                                    if (!mCurrentSection.equals(SECTION_PRO_ORGANIZATIONS)) {
                                        mCurrentSection = SECTION_PRO_ORGANIZATIONS;
                                        getSupportFragmentManager().beginTransaction()
                                                .replace(R.id.frameLayoutUserMain,
                                                        ProOrganizationsFragment
                                                                .newInstance(mProfessional))
                                                .addToBackStack(null).commit();
                                    }
                                    return true;
                                case R.id.buttonProNavProfile:
                                    if (!mCurrentSection.equals(SECTION_PRO_PROFILE)) {
                                        mCurrentSection = SECTION_PRO_PROFILE;
                                        getSupportFragmentManager().beginTransaction()
                                                .replace(R.id.frameLayoutUserMain,
                                                        ProfileFragment.newInstance(mProfessional))
                                                .addToBackStack(null).commit();
                                    }
                                    return true;
                            }
                            return false;
                        }
                    });
        } else {
            mBottomNavigationView.setOnNavigationItemSelectedListener(
                    new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.buttonAdminNavCredentials:
                            if (!mCurrentSection.equals(SECTION_ADMIN_CREDENTIALS)) {
                                mCurrentSection = SECTION_ADMIN_CREDENTIALS;
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.frameLayoutUserMain,
                                                AdministratorCredentialsFragment
                                                        .newInstance(mAdministrator))
                                        .addToBackStack(null).commit();
                            }
                            return true;
                        case R.id.buttonAdminNavMyOrganization:
                            if (!mCurrentSection.equals(SECTION_ADMIN_ORGANIZATION)) {
                                mCurrentSection = SECTION_ADMIN_ORGANIZATION;
                                if (mAdministrator.getPrimaryAdminOrganizationId() != null &&
                                        !mAdministrator.getPrimaryAdminOrganizationId().isEmpty()) {
                                    mDatabase.child(DatabaseContracts.ORGANIZATIONS)
                                            .child(mAdministrator.getPrimaryAdminOrganizationId())
                                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                                @SuppressWarnings("unchecked")
                                                @Override
                                                public void onDataChange(
                                                        @NonNull DataSnapshot dataSnapshot) {
                                                    String id = mAdministrator
                                                            .getPrimaryAdminOrganizationId();
                                                    String name = (String) dataSnapshot
                                                            .child(DatabaseContracts.NAME).getValue();
                                                    ArrayList<String> primaryAdmins =
                                                            (ArrayList<String>) dataSnapshot
                                                                    .child(DatabaseContracts
                                                                            .PRIMARY_ADMINS)
                                                                    .getValue();
                                                    ArrayList<String> secondaryAdmins =
                                                            (ArrayList<String>) dataSnapshot.
                                                                    child(DatabaseContracts
                                                                            .SECONDARY_ADMINS)
                                                                    .getValue();
                                                    ArrayList<String> professionals =
                                                            (ArrayList<String>) dataSnapshot
                                                                    .child(DatabaseContracts
                                                                            .ROSTER_PROFESSIONALS)
                                                                    .getValue();

                                                    Organization org = new Organization(id, name,
                                                            primaryAdmins, secondaryAdmins,
                                                            professionals);

                                                    getSupportFragmentManager().beginTransaction()
                                                            .replace(R.id.frameLayoutUserMain,
                                                                    AdminMyOrganizationFragment
                                                                            .newInstance(org,
                                                                                    mAdministrator))
                                                            .addToBackStack(null).commit();
                                                }

                                                @Override
                                                public void onCancelled(
                                                        @NonNull DatabaseError databaseError) {
                                                    // Nothing
                                                }
                                            });
                                } else {
                                    getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.frameLayoutUserMain,
                                                    AdminMyOrganizationFragment.newInstance(null,
                                                            mAdministrator))
                                            .addToBackStack(null).commit();
                                }
                            }
                            return true;
                        case R.id.buttonAdminNavRoster:
                            if (!mCurrentSection.equals(SECTION_ADMIN_ROSTER)) {
                                mCurrentSection = SECTION_ADMIN_ROSTER;
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.frameLayoutUserMain,
                                                AdminRosterFragment.newInstance(mAdministrator))
                                        .addToBackStack(null).commit();
                            }
                            return true;
                        case R.id.buttonAdminNavMyProfile:
                            if (!mCurrentSection.equals(SECTION_ADMIN_PROFILE)) {
                                mCurrentSection = SECTION_ADMIN_PROFILE;
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.frameLayoutUserMain,
                                                ProfileFragment.newInstance(mAdministrator))
                                        .addToBackStack(null).commit();
                            }
                            return true;
                    }
                    return false;
                }
            });
        }
    }
}
