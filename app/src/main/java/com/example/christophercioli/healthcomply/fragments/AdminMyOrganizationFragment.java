// Christopher Cioli
// P&P6 - 1807
// AdminMyOrganizationFragment.java

package com.example.christophercioli.healthcomply.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.christophercioli.healthcomply.R;
import com.example.christophercioli.healthcomply.activities.UserMainActivity;
import com.example.christophercioli.healthcomply.contracts.DatabaseContracts;
import com.example.christophercioli.healthcomply.models.Administrator;
import com.example.christophercioli.healthcomply.models.Organization;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

public class AdminMyOrganizationFragment extends Fragment {

    // UI Elements
    private FrameLayout mFrameLayoutCreateOrganization;
    private EditText mEditTextNewOrganizationName;
    private Button mButtonCreateNewOrganization;
    private LinearLayout mLinearLayoutOrganizationDetails;
    private TextView mTextViewOrganizationName;
    private TextView mTextViewOrganizationPrimaryAdministrators;
    private TextView mTextViewOrganizationSecondaryAdministrators;
    private Button mButtonGenerateJoinCode;

    // Constants
    private static final String ARG_ORGANIZATION = "ARG_ORGANIZATION";
    private static final String ARG_ADMINISTRATOR = "ARG_ADMINISTRATOR";

    // Member Variables
    private Administrator mAdministrator;
    private Organization mOrganization;
    private ArrayList<String> mPrimaryAdministrators = new ArrayList<>();
    private ArrayList<String> mSecondaryAdministrators = new ArrayList<>();
    private DatabaseReference mDatabase;
    private FirebaseUser mFirebaseUser;

    // Required Empty Constructor
    public AdminMyOrganizationFragment() { }

    public static AdminMyOrganizationFragment newInstance(Organization _org, Administrator _admin) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_ORGANIZATION, _org);
        args.putSerializable(ARG_ADMINISTRATOR, _admin);

        AdminMyOrganizationFragment fragment = new AdminMyOrganizationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_admin_my_organization, container,
                false);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() instanceof UserMainActivity) {
            UserMainActivity activity = (UserMainActivity) getActivity();
            activity.mCurrentSection = UserMainActivity.SECTION_ADMIN_ORGANIZATION;
            BottomNavigationView bottomNav = activity.findViewById(R.id.bottomNavUserMain);
            bottomNav.setSelectedItemId(R.id.buttonAdminNavMyOrganization);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if (getArguments() != null) {
            mOrganization = (Organization) getArguments().getSerializable(ARG_ORGANIZATION);
            mAdministrator = (Administrator) getArguments().getSerializable(ARG_ADMINISTRATOR);

            setUpUI();
        }
    }

    // Fragment Functionality
    private void setCreateButtonOnClickListener() {
        mButtonCreateNewOrganization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEditTextNewOrganizationName.getText().toString().isEmpty()) {
                    mEditTextNewOrganizationName.setError("Organization Name can't be empty.");
                } else {
                    String orgUUID = UUID.randomUUID().toString();
                    String name = mEditTextNewOrganizationName.getText().toString();
                    ArrayList<String> primaryAdmins = new ArrayList<>();
                    primaryAdmins.add(mFirebaseUser.getUid());
                    mOrganization = new Organization(orgUUID, name, primaryAdmins,
                            null, null);
                    mAdministrator.setPrimaryAdminOrganizations(orgUUID);
                    mDatabase.child(DatabaseContracts.USERS).child(mFirebaseUser.getUid())
                            .setValue(mAdministrator);
                    mDatabase.child(DatabaseContracts.ORGANIZATIONS).child(orgUUID)
                            .setValue(mOrganization);


                    Toast.makeText(getContext(), "Organization Created!", Toast.LENGTH_LONG)
                            .show();

                    setUpUI();
                }
            }
        });
    }

    private void setJoinCodeButtonOnClickListener() {
        mButtonGenerateJoinCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String baseString = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                Random random = new Random();
                StringBuilder joinCodeBuilder = new StringBuilder(8);

                for (int i = 0; i < 8; i++) {
                    joinCodeBuilder.append(baseString.charAt(random.nextInt(baseString.length())));
                }

                final String joinCode = joinCodeBuilder.toString();

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Share Join Code")
                        .setMessage("Join Code Created!\nTap \"OK\" " +
                                "to share the join code via email.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mDatabase.child(DatabaseContracts.JOIN_CODES)
                                        .child(mOrganization.getId()).push().setValue(joinCode);

                                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                                shareIntent.setType("message/rfc822");
                                shareIntent.putExtra(Intent.EXTRA_SUBJECT, String
                                        .format("Invite to Join %s on Health Comply",
                                                mOrganization.getName()));
                                String bodyString = "You've been invited to join " +
                                        mOrganization.getName() + " ";
                                bodyString += "on Health Comply! Please copy the code below. " +
                                        "Use the code in the ";
                                bodyString += "\"My Organization\" tab in the Health Comply app " +
                                        "after tapping ";
                                bodyString += "the \"Join Organization\" button.";
                                bodyString += "\n\n Join Code:     " + joinCode;
                                bodyString += "\n\nThanks!\nThe Health Comply Team";
                                shareIntent.putExtra(Intent.EXTRA_TEXT, bodyString);
                                startActivity(Intent.createChooser(shareIntent,
                                        "Share Join Code to..."));
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // Do Nothing
                            }
                        }).show();
            }
        });
    }

    private String getAdminString(ArrayList<String> _adminList) {
        String outputString = "";
        for (String s : _adminList) {
            outputString += String.format("%s, ", s);
        }

        outputString = outputString.substring(0, outputString.length() - 2);
        return outputString;
    }

    private void getAdministratorSets() {
        if (mOrganization != null) {
            if (mOrganization.getPrimaryAdministratorIds() != null &&
                    !mOrganization.getPrimaryAdministratorIds().isEmpty()) {
                mDatabase.child(DatabaseContracts.USERS).addListenerForSingleValueEvent(
                        new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot c : dataSnapshot.getChildren()) {
                            if (mOrganization.getPrimaryAdministratorIds().contains(c.getKey())) {
                                mPrimaryAdministrators.clear();

                                String firstName = (String) c.child(DatabaseContracts.FIRST_NAME)
                                        .getValue();
                                String lastName = (String) c.child(DatabaseContracts.LAST_NAME)
                                        .getValue();

                                mPrimaryAdministrators.add(String.format("%s %s", firstName,
                                        lastName));
                            }
                        }

                        if (mPrimaryAdministrators.size() == 0) {
                            mTextViewOrganizationPrimaryAdministrators
                            .setText(R.string.noPrimaryAdmins);
                            mTextViewOrganizationPrimaryAdministrators.setTypeface(
                                    mTextViewOrganizationPrimaryAdministrators.getTypeface(),
                                    Typeface.ITALIC);
                        } else {
                            mTextViewOrganizationPrimaryAdministrators
                                    .setText(getAdminString(mPrimaryAdministrators));
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // Nothing
                    }
                });
            }

            if (mOrganization.getSecondaryAdministratorIds() != null &&
                    !mOrganization.getSecondaryAdministratorIds().isEmpty()) {
                mDatabase.child(DatabaseContracts.USERS).addListenerForSingleValueEvent(
                        new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot c : dataSnapshot.getChildren()) {
                            if (mOrganization.getSecondaryAdministratorIds().contains(c.getKey())) {
                                String firstName = (String) c.child(DatabaseContracts.FIRST_NAME)
                                        .getValue();
                                String lastName = (String) c.child(DatabaseContracts.LAST_NAME)
                                        .getValue();

                                mSecondaryAdministrators.add(String.format("%s %s", firstName,
                                        lastName));
                            }
                        }

                        if (mSecondaryAdministrators.size() == 0) {
                            mTextViewOrganizationSecondaryAdministrators
                                    .setText(R.string.noSecondaryAdmins);
                            mTextViewOrganizationSecondaryAdministrators.setTypeface(
                                    mTextViewOrganizationSecondaryAdministrators.getTypeface(),
                                    Typeface.ITALIC);
                        } else {
                            mTextViewOrganizationSecondaryAdministrators
                                    .setText(getAdminString(mSecondaryAdministrators));
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // Nothing
                    }
                });
            } else {
                mTextViewOrganizationSecondaryAdministrators
                        .setText(R.string.noSecondaryAdmins);
                mTextViewOrganizationSecondaryAdministrators.setTypeface(
                        mTextViewOrganizationSecondaryAdministrators.getTypeface(),
                        Typeface.ITALIC);
            }
        }
    }

    private void setUpUI() {
        if (getActivity() != null) {
            mLinearLayoutOrganizationDetails = getActivity()
                    .findViewById(R.id.linearLayoutOrganizationDetails);
            mFrameLayoutCreateOrganization = getActivity()
                    .findViewById(R.id.frameLayoutCreateOrganization);

            if (mOrganization == null) {
                mFrameLayoutCreateOrganization.setVisibility(View.VISIBLE);
                mLinearLayoutOrganizationDetails.setVisibility(View.GONE);

                mEditTextNewOrganizationName = getActivity()
                        .findViewById(R.id.editTextNewOrganizationName);
                mButtonCreateNewOrganization = getActivity()
                        .findViewById(R.id.buttonCreateNewOrganization);

                setCreateButtonOnClickListener();
            } else {
                mFrameLayoutCreateOrganization.setVisibility(View.GONE);
                mLinearLayoutOrganizationDetails.setVisibility(View.VISIBLE);

                mTextViewOrganizationName = getActivity()
                        .findViewById(R.id.textViewOrganizationName);
                mTextViewOrganizationPrimaryAdministrators = getActivity()
                        .findViewById(R.id.textViewOrganizationPrimaryAdministrators);
                mTextViewOrganizationSecondaryAdministrators = getActivity()
                        .findViewById(R.id.textViewOrganizationSecondaryAdministrators);
                mButtonGenerateJoinCode = getActivity()
                        .findViewById(R.id.buttonGenerateJoinCode);

                mTextViewOrganizationName.setText(mOrganization.getName());
                getAdministratorSets();
                setJoinCodeButtonOnClickListener();
            }
        }
    }
}
