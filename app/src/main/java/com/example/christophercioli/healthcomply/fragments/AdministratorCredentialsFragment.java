// Christopher Cioli
// P&P6 - 1807
// AdministratorCredentialsFragment.java

package com.example.christophercioli.healthcomply.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.christophercioli.healthcomply.R;
import com.example.christophercioli.healthcomply.activities.UserMainActivity;
import com.example.christophercioli.healthcomply.adapters.AdminCredentialAdapter;
import com.example.christophercioli.healthcomply.adapters.ProCredentialAdapter;
import com.example.christophercioli.healthcomply.contracts.DatabaseContracts;
import com.example.christophercioli.healthcomply.models.AdminCredential;
import com.example.christophercioli.healthcomply.models.Administrator;
import com.example.christophercioli.healthcomply.models.Organization;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

public class AdministratorCredentialsFragment extends Fragment {

    // UI Elements
    private RecyclerView mRecyclerViewAdminCredentials;

    // Member Variables
    private Administrator mAdministrator;
    private Organization mOrganization;
    private ArrayList<AdminCredential> mAdminCredentials;
    private DatabaseReference mDatabase;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private FragmentActivity mFragmentActivity;

    // Constants
    private static final String ARG_ADMINISTRATOR = "ARG_ADMINISTRATOR";

    // Required Empty Constructor
    public AdministratorCredentialsFragment() { }

    public static AdministratorCredentialsFragment newInstance(Administrator _admin) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_ADMINISTRATOR, _admin);

        AdministratorCredentialsFragment fragment = new AdministratorCredentialsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mFragmentActivity = (FragmentActivity) context;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() instanceof UserMainActivity) {
            UserMainActivity activity = (UserMainActivity) getActivity();
            activity.mCurrentSection = UserMainActivity.SECTION_ADMIN_CREDENTIALS;
            BottomNavigationView bottomNav = activity.findViewById(R.id.bottomNavUserMain);
            bottomNav.setSelectedItemId(R.id.buttonAdminNavCredentials);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_administrator_credentials, container,
                false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments() != null) {
            mAdministrator = (Administrator) getArguments().getSerializable(ARG_ADMINISTRATOR);
        }

        if (getActivity() != null) {
            mRecyclerViewAdminCredentials = getActivity()
                    .findViewById(R.id.recyclerViewAdminCredentials);
            mRecyclerViewAdminCredentials.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerViewAdminCredentials.setLayoutManager(mLayoutManager);
        }

        mAdminCredentials = new ArrayList<>();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        if (mAdministrator.getPrimaryAdminOrganizationId() != null) {
            mDatabase.child(DatabaseContracts.ORGANIZATIONS)
                    .child(mAdministrator.getPrimaryAdminOrganizationId())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @SuppressWarnings("unchecked")
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            String id = mAdministrator.getPrimaryAdminOrganizationId();
                            String name = (String) dataSnapshot.child(DatabaseContracts.NAME)
                                    .getValue();
                            ArrayList<String> primaryAdmins = (ArrayList<String>) dataSnapshot
                                    .child(DatabaseContracts.PRIMARY_ADMINS).getValue();
                            ArrayList<String> secondaryAdmins = (ArrayList<String>) dataSnapshot
                                    .child(DatabaseContracts.SECONDARY_ADMINS).getValue();
                            ArrayList<String> roster = (ArrayList<String>) dataSnapshot
                                    .child(DatabaseContracts.ROSTER_PROFESSIONALS).getValue();

                            mOrganization = new Organization(id, name, primaryAdmins,
                                    secondaryAdmins, roster);

                            loadAdminCredentials();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            // Nothing
                        }
                    });
        }
    }

    // Fragment Functionality
    private void loadAdminCredentials() {
        if (mOrganization.getRosterProfessionalIds() != null) {
            mDatabase.child(DatabaseContracts.USERS).addListenerForSingleValueEvent(
                    new ValueEventListener() {
                        @SuppressWarnings("unchecked")
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (DataSnapshot c : dataSnapshot.getChildren()) {
                                if (mOrganization.getRosterProfessionalIds().contains(c.getKey())) {
                                    final String id = c.getKey();
                                    final String fullName = c.child(DatabaseContracts.FIRST_NAME).getValue() + " "
                                            + c.child(DatabaseContracts.LAST_NAME).getValue();
                                    final ArrayList<String> credentials = (ArrayList<String>)
                                            c.child(DatabaseContracts.CREDENTIAL_IDS).getValue();

                                    mDatabase.child(DatabaseContracts.CREDENTIALS)
                                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    for (DataSnapshot d : dataSnapshot.getChildren()) {
                                                        if (credentials.contains(d.getKey())) {
                                                            String innerCredId = d.getKey();
                                                            String title = (String) d.child(DatabaseContracts.TITLE)
                                                                    .getValue();
                                                            String credentialId = (String)
                                                                    d.child(DatabaseContracts.CREDENTIAL_ID).getValue();
                                                            String certifyingBody = (String)
                                                                    d.child(DatabaseContracts.CERTIFYING_BODY)
                                                                            .getValue();
                                                            String expirationDate = (String)
                                                                    d.child(DatabaseContracts.EXPIRATION_DATE)
                                                                            .getValue();
                                                            String imageId = (String)
                                                                    d.child(DatabaseContracts.IMAGE_ID).getValue();

                                                            AdminCredential newAdminCred =
                                                                    new AdminCredential(innerCredId, title,
                                                                            credentialId, certifyingBody,
                                                                            expirationDate, imageId, id, fullName);

                                                            mAdminCredentials.add(newAdminCred);
                                                        }
                                                    }

                                                    Collections.sort(mAdminCredentials);
                                                    mAdapter = new AdminCredentialAdapter(mAdminCredentials, getActivity(),
                                                            mFragmentActivity);
                                                    mRecyclerViewAdminCredentials.setAdapter(mAdapter);
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                    // Nothing
                                                }
                                            });
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            // Nothing
                        }
                    });
        }
    }
}
