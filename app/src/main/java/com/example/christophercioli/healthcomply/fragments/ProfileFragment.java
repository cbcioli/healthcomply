// Christopher Cioli
// P&P6 - 1807
// ProfileFragment.java

package com.example.christophercioli.healthcomply.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.christophercioli.healthcomply.R;
import com.example.christophercioli.healthcomply.activities.LaunchActivity;
import com.example.christophercioli.healthcomply.activities.UserMainActivity;
import com.example.christophercioli.healthcomply.models.User;
import com.google.firebase.auth.FirebaseAuth;

public class ProfileFragment extends Fragment {

    // UI Elements
    private TextView mTextViewFullName;
    private TextView mTextViewTitle;
    private TextView mTextViewEmailAddress;
    private Button mButtonLogOut;

    // Member Variables
    private User mUser;

    // Constants
    private static final String ARG_USER = "ARG_USER";

    // Required Empty Constructor
    public ProfileFragment() { }

    public static ProfileFragment newInstance(User _user) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_USER, _user);

        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() instanceof UserMainActivity) {
            UserMainActivity activity = (UserMainActivity) getActivity();
            BottomNavigationView bottomNav = activity.findViewById(R.id.bottomNavUserMain);

            if (mUser.getIsAdmin()) {

                activity.mCurrentSection = UserMainActivity.SECTION_ADMIN_PROFILE;
                bottomNav.setSelectedItemId(R.id.buttonAdminNavMyProfile);
            } else {
                activity.mCurrentSection = UserMainActivity.SECTION_PRO_PROFILE;
                bottomNav.setSelectedItemId(R.id.buttonProNavProfile);
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments() != null) {
            mUser = (User) getArguments().getSerializable(ARG_USER);
        }

        if (getActivity() != null) {
            mTextViewFullName = getActivity().findViewById(R.id.textViewProfileFullName);
            mTextViewTitle = getActivity().findViewById(R.id.textViewProfileTitle);
            mTextViewEmailAddress = getActivity().findViewById(R.id.textViewProfileEmailAddress);
            mButtonLogOut = getActivity().findViewById(R.id.buttonLogOut);

            setUserDetails();
            setUpButtonOnClickListener();
        }
    }

    // Fragment Functionality
    private void setUserDetails() {
        mTextViewFullName.setText(String.format("%s %s", mUser.getFirstName(),
                mUser.getLastName()));
        mTextViewTitle.setText(mUser.getTitle());
        mTextViewEmailAddress.setText(mUser.getEmailAddress());
    }
    private void setUpButtonOnClickListener() {
        mButtonLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder b = new AlertDialog.Builder(getContext());
                b.setTitle("Log Out")
                        .setMessage("Are you sure you want to log out of Health Comply?")
                        .setNegativeButton("No", null)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                FirebaseAuth.getInstance().signOut();
                                Intent signOutIntent = new Intent(getActivity(),
                                        LaunchActivity.class);
                                signOutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                        |Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(signOutIntent);
                            }
                        }).show();
            }
        });
    }
}
