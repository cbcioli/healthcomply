// Christopher Cioli
// P&P6 - 1807
// DatabaseContracts.java

package com.example.christophercioli.healthcomply.contracts;

public class DatabaseContracts {
    // Model Constants
    public static String USERS = "users";
    public static String ORGANIZATIONS = "organizations";
    public static String CREDENTIALS = "credentials";
    public static String ID = "id";
    public static String JOIN_CODES = "joinCodes";

    // Professional & Administrator Constants
    public static String FIRST_NAME = "firstName";
    public static String LAST_NAME = "lastName";
    public static String EMAIL_ADDRESS = "emailAddress";
    public static String TITLE = "title";
    public static String IS_ADMIN = "isAdmin";
    public static String ORGANIZATION_IDS = "organizationIds";
    public static String PRIMARY_ADMIN_ORG = "primaryAdminOrganizationId";
    public static String SECONDARY_ADMIN_ORGS = "secondaryAdminOrgs";
    public static String CREDENTIAL_IDS = "credentialIds";

    // Credential Constants
    public static String CERTIFYING_BODY = "certifyingBody";
    public static String CREDENTIAL_ID = "credentialId";
    public static String EXPIRATION_DATE = "expirationDate";
    public static String IMAGE_ID = "image";

    // Organization Constants
    public static String NAME = "name";
    public static String PRIMARY_ADMINS = "primaryAdministratorIds";
    public static String SECONDARY_ADMINS = "secondaryAdministratorIds";
    public static String ROSTER_PROFESSIONALS = "rosterProfessionalIds";
}
