// Christopher Cioli
// P&P6 - 1807
// Professional.java

package com.example.christophercioli.healthcomply.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Professional extends User implements Serializable {
    // Member Variables
    private ArrayList<String> credentials;
    private ArrayList<String> organizations;

    public Professional(String _id, String _firstName, String _lastName, String _emailAddress,
                        String _title, boolean _isAdmin, ArrayList<String> _credentialIds,
                        ArrayList<String> _organizationIds) {
        super(_id, _firstName, _lastName, _emailAddress, _title, _isAdmin);

        credentials = _credentialIds;
        organizations = _organizationIds;
    }

    // Getters
    public ArrayList<String> getCredentialIds() {
        return credentials;
    }

    public ArrayList<String> getOrganizationIds() {
        return organizations;
    }

    // Setters
    public void setCredentials(ArrayList<String> credentials) {
        this.credentials = credentials;
    }

    public void setOrganizations(ArrayList<String> _organizations) {
        organizations = _organizations;
    }
}
