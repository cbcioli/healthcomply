// Christopher Cioli
// P&P6 - 1807
// AddNewCredentialFragment.java

package com.example.christophercioli.healthcomply.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.example.christophercioli.healthcomply.R;
import com.example.christophercioli.healthcomply.activities.ImageCropActivity;
import com.example.christophercioli.healthcomply.contracts.DatabaseContracts;
import com.example.christophercioli.healthcomply.models.Credential;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import me.pqpo.smartcropperlib.view.CropImageView;

import static android.app.Activity.RESULT_OK;

public class AddNewCredentialFragment extends Fragment {

    // UI Elements
    private TextView mTextViewFragmentTitle;
    private EditText mEditTextNewCredentialTitle;
    private EditText mEditTextNewCredentialId;
    private EditText mEditTextNewCredentialCertifyingBody;
    private ImageView mImageViewNewCredentialImage;
    private Button mButtonSetExpirationDate;
    private Button mButtonStoreCredentialPicture;
    private Button mButtonSave;
    private TextView mTextViewExpirationDate;

    // Member Variables
    private boolean mIsEditing;
    private boolean mHasChangedImages = false;
    private Credential mEditCredential;
    private GlideDrawable mGlideDrawable;
    private DatabaseReference mDatabase;
    private StorageReference mStorage;
    private Bitmap mCredentialImage = null;
    private String mCredentialImageId;
    private Uri mCredentialImageUri;

    // Constants
    public static final String ARG_IS_EDITING = "ARG_IS_EDITING";
    public static final String ARG_EDIT_CREDENTIAL = "ARG_EDIT_CREDENTIAL";
    public static final String EXTRA_EDITED_CREDENTIAL = "EXTRA_EDITED_CREDENTIAL";
    public static final int CROP_IMAGE_REQUEST = 0x0110;
    public static final int CROP_IMAGE_PERMISSIONS_REQUEST = 0x1110;
    public static final String EXTRA_NEW_CREDENTIAL = "EXTRA_NEW_CREDENTIAL";
    private static final String TITLE = "title";
    private static final String CERTIFYING_BODY = "certifyingBody";
    private static final String EXPIRATION_DATE = "expirationDate";

    // Required Empty Constructor
    public AddNewCredentialFragment() { }

    public static AddNewCredentialFragment newInstance(boolean _isEditing,
                                                       Credential _editCredential) {

        Bundle args = new Bundle();
        args.putBoolean(ARG_IS_EDITING, _isEditing);
        args.putSerializable(ARG_EDIT_CREDENTIAL, _editCredential);

        AddNewCredentialFragment fragment = new AddNewCredentialFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_new_credential, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorage = FirebaseStorage.getInstance().getReference();

        if (getArguments() != null) {
            mIsEditing = getArguments().getBoolean(ARG_IS_EDITING);
            mEditCredential = (Credential) getArguments().getSerializable(ARG_EDIT_CREDENTIAL);
        }

        if (getActivity() != null) {
            mTextViewFragmentTitle = getActivity().findViewById(R.id.textViewAddEditFragmentTitle);
            mButtonSetExpirationDate = getActivity().findViewById(R.id.buttonSetExpirationDate);
            mButtonStoreCredentialPicture = getActivity()
                    .findViewById(R.id.buttonSetCredentialImage);
            mButtonSave = getActivity().findViewById(R.id.buttonSaveNewCredential);
            mEditTextNewCredentialTitle = getActivity()
                    .findViewById(R.id.editTextNewCredentialTitle);
            mEditTextNewCredentialId = getActivity().findViewById(R.id.editTextNewCredentialId);
            mEditTextNewCredentialCertifyingBody = getActivity()
                    .findViewById(R.id.editTextNewCredentialCertifyingBody);
            mImageViewNewCredentialImage = getActivity()
                    .findViewById(R.id.imageViewCredentialImage);
            mTextViewExpirationDate = getActivity().findViewById(R.id.textViewExpireDate);

            setButtonOnClickListeners();
        }

        if (mIsEditing) {
            mTextViewFragmentTitle.setText(R.string.editCredential);
            mButtonSetExpirationDate.setText(R.string.changeExpirationDate);
            mButtonStoreCredentialPicture.setText(R.string.changeCredentialPicture);
            mEditTextNewCredentialTitle.setText(mEditCredential.getTitle());
            mEditTextNewCredentialId.setText(mEditCredential.getCredentialId());
            mEditTextNewCredentialCertifyingBody.setText(mEditCredential.getCertifyingBody());
            mTextViewExpirationDate.setText(mEditCredential.getExpirationDate());
            if (mEditCredential.getImage() != null && !mEditCredential.getImage().isEmpty()) {
                GlideDrawableImageViewTarget target =
                        new GlideDrawableImageViewTarget(mImageViewNewCredentialImage);
                Glide.with(getContext()).using(new FirebaseImageLoader())
                        .load(mStorage.child("user-images").child(mEditCredential.getImage()))
                        .thumbnail(Glide.with(getContext()).load(R.drawable.blocks_1s_72px))
                        .listener(new RequestListener<StorageReference, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, StorageReference model,
                                                       Target<GlideDrawable> target,
                                                       boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource,
                                                           StorageReference model,
                                                           Target<GlideDrawable> target,
                                                           boolean isFromMemoryCache,
                                                           boolean isFirstResource) {
                                mGlideDrawable = resource;
                                return false;
                            }
                        })
                        .into(target);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CROP_IMAGE_REQUEST && resultCode == RESULT_OK) {
            String filePath = data
                    .getStringExtra(ImageCropActivity.EXTRA_CROPPED_IMAGE_PATH);
            mCredentialImageUri = Uri.fromFile(new File(filePath));
            mCredentialImage = BitmapFactory.decodeFile(filePath);

            mImageViewNewCredentialImage.setImageBitmap(mCredentialImage);
            mHasChangedImages = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CROP_IMAGE_PERMISSIONS_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                launchImageCropActivity();
            }
        }
    }

    // Fragment Functionality
    private void setButtonOnClickListeners() {
        mButtonSetExpirationDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() != null) {
                    DialogFragment datePickerFragment = new DatePickerFragment();
                    datePickerFragment.show(getActivity().getSupportFragmentManager(),
                            "picker");
                }
            }
        });

        mButtonStoreCredentialPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() != null) {
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[] {
                                Manifest.permission.CAMERA }, CROP_IMAGE_PERMISSIONS_REQUEST);
                    } else {
                        launchImageCropActivity();
                    }
                }
            }
        });

        mButtonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateCredentialData()) {
                    final ProgressDialog pd = new ProgressDialog(getContext());
                    pd.setMessage("Saving Your New Credential...");
                    pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    pd.show();

                    final String expirationDate = mTextViewExpirationDate.getText().toString();
                    final String title = mEditTextNewCredentialTitle.getText().toString();
                    final String certifyingBody =
                            mEditTextNewCredentialCertifyingBody.getText().toString();
                    final String credentialId = mEditTextNewCredentialId.getText().toString();
                    final String id = UUID.randomUUID().toString();

                    if (mHasChangedImages) {
                        if (mEditCredential != null && mEditCredential.getImage() != null) {
                            mStorage.child("user-images/" + mEditCredential.getImage()).delete();
                        }

                        mCredentialImageId = UUID.randomUUID().toString();
                        StorageReference ref = mStorage.child("user-images/" + mCredentialImageId);
                        ref.putFile(mCredentialImageUri).addOnSuccessListener(
                                new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                        Credential newCredential;
                                        if (mIsEditing) {
                                            newCredential = new Credential(mEditCredential.getId(),
                                                    title, credentialId, certifyingBody,
                                                    expirationDate, mCredentialImageId);
                                            mDatabase.child(DatabaseContracts.CREDENTIALS)
                                                    .child(mEditCredential.getId())
                                                    .setValue(newCredential);
                                        } else {
                                            newCredential = new Credential(id, title,
                                                    credentialId, certifyingBody, expirationDate,
                                                    mCredentialImageId);
                                            mDatabase.child(DatabaseContracts.CREDENTIALS).child(id)
                                                    .setValue(newCredential);
                                        }


                                        sendNewCredentialBack(newCredential);
                                        pd.dismiss();
                                    }
                                });
                    } else {
                        Credential newCredential;
                        if (mIsEditing) {
                            newCredential = new Credential(mEditCredential.getId(), title,
                                    credentialId, certifyingBody, expirationDate,
                                    mEditCredential.getImage());
                        } else {
                            newCredential = new Credential(id, title, credentialId,
                                    certifyingBody, expirationDate, null);
                        }

                        mDatabase.child(DatabaseContracts.CREDENTIALS).child(id)
                                .setValue(newCredential);

                        sendNewCredentialBack(newCredential);
                    }
                }
            }
        });
    }

    private void launchImageCropActivity() {
        Intent cropIntent = new Intent(getActivity(), ImageCropActivity.class);
        startActivityForResult(cropIntent, CROP_IMAGE_REQUEST);
    }

    private boolean validateCredentialData() {
        boolean isValid = false;
        String title = mEditTextNewCredentialTitle.getText().toString();
        String certifyingBody = mEditTextNewCredentialCertifyingBody.getText().toString();
        String expirationDate = mTextViewExpirationDate.getText().toString();

        if (!expirationDate.equals(getResources().getString(R.string.noDateSelected)) &&
                !title.isEmpty() && !certifyingBody.isEmpty()) {
            isValid = true;
        } else {
            if (expirationDate.equals(getResources().getString(R.string.noDateSelected))) {
                setErrorState(EXPIRATION_DATE);
            }

            if (certifyingBody.isEmpty()) {
                setErrorState(CERTIFYING_BODY);
            }

            if (title.isEmpty()) {
                setErrorState(TITLE);
            }
        }

        return isValid;
    }

    private void setErrorState(String _dataInError) {
        switch(_dataInError) {
            case TITLE:
                mEditTextNewCredentialTitle.setError("Title must not be blank.");
                break;
            case CERTIFYING_BODY:
                mEditTextNewCredentialCertifyingBody.setError("Certifying Body must not be blank.");
                break;
            case EXPIRATION_DATE:
                Toast.makeText(getActivity(), "Please set an expiration date",
                        Toast.LENGTH_LONG).show();
                break;
        }
    }

    private void sendNewCredentialBack(Credential _newCredential) {
        Intent resultIntent = new Intent();
        if (!mIsEditing) {
            resultIntent.putExtra(EXTRA_NEW_CREDENTIAL, _newCredential);
        } else {
            resultIntent.putExtra(EXTRA_EDITED_CREDENTIAL, _newCredential);
        }
        if (getActivity() != null) {
            getActivity().setResult(RESULT_OK, resultIntent);
            getActivity().finish();
        }
    }
}
