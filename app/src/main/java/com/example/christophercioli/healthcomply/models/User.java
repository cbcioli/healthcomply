// Christopher Cioli
// P&P6 - 1807
// User.java

package com.example.christophercioli.healthcomply.models;

import java.io.Serializable;

public abstract class User implements Serializable {
    // Member Variables
    private String id;
    private String first_name;
    private String last_name;
    private String email_address;
    private String title;
    private boolean is_admin;

    public User(String _id, String _firstName, String _lastName, String _emailAddress, String _title,
                boolean _isAdmin) {
        id = _id;
        first_name = _firstName;
        last_name = _lastName;
        email_address = _emailAddress;
        title = _title;
        is_admin = _isAdmin;
    }

    // Getters
    public String getId() {
        return id;
    }

    public String getFirstName() {
        return first_name;
    }

    public String getLastName() {
        return last_name;
    }

    public String getTitle() {
        return title;
    }

    public String getEmailAddress() {
        return email_address;
    }

    public boolean getIsAdmin() {
        return is_admin;
    }

    // Setters
    public void setId(String _id) {
        id = _id;
    }

    public void setFirstName(String _firstName) {
        first_name = _firstName;
    }

    public void setLastName(String _lastName) {
        last_name = _lastName;
    }

    public void setTitle(String _title) {
        title = _title;
    }

    public void setEmailAddress(String _emailAddress) {
        email_address = _emailAddress;
    }

    public void setIsAdmin(boolean _isAdmin) {
        is_admin = _isAdmin;
    }
}
