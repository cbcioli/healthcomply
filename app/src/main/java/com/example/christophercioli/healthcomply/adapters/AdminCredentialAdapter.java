// Christopher Cioli
// P&P6 - 1807
// AdminCredentialAdapter.java

package com.example.christophercioli.healthcomply.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.christophercioli.healthcomply.R;
import com.example.christophercioli.healthcomply.fragments.ProCredentialDetailFragment;
import com.example.christophercioli.healthcomply.models.AdminCredential;
import com.example.christophercioli.healthcomply.models.Credential;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class AdminCredentialAdapter extends RecyclerView.Adapter<AdminCredentialAdapter.ViewHolder> {

    // Member Variables
    private ArrayList<AdminCredential> mDataset;
    private Activity mActivity;
    private FragmentActivity mFragmentActivity;

    public AdminCredentialAdapter(ArrayList<AdminCredential> _credentials, Activity _a,
                                  FragmentActivity _fragmentActivity) {
        mDataset = _credentials;
        mActivity = _a;
        mFragmentActivity = _fragmentActivity;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View credentialCell = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_admin_credential, parent, false);
        return new AdminCredentialAdapter.ViewHolder(credentialCell);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final AdminCredential cred = mDataset.get(position);
        holder.mTextViewProName.setText(cred.getProFullName());
        holder.mTextViewTitle.setText(cred.getTitle());
        holder.mTextViewExpirationDate
                .setText(String.format("Expires: %s", cred.getExpirationDate()));

        Date today = new Date();
        long diff = cred.findDateTime().getTime() - today.getTime();
        int daysDiff = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

        if (daysDiff < 1) {
            // EXPIRED
            holder.mLinearLayout.setBackgroundColor(mActivity.getResources()
                    .getColor(R.color.red));
            holder.mTextViewProName.setTextColor(mActivity.getResources()
                    .getColor(R.color.white));
            holder.mTextViewExpirationDate.setTextColor(mActivity.getResources()
                    .getColor(R.color.white));
            holder.mTextViewTitle.setTextColor(mActivity.getResources()
                    .getColor(R.color.white));
            holder.mTextViewStatus.setText(R.string.expiredCaps);
            holder.mTextViewStatus.setTextColor(mActivity.getResources()
                    .getColor(R.color.white));
        } else if (daysDiff >= 1 && daysDiff <= 30) {
            // 30 Days out
            holder.mTextViewProName.setTextColor(mActivity.getResources()
                    .getColor(R.color.red));
            holder.mTextViewExpirationDate.setTextColor(mActivity.getResources()
                    .getColor(R.color.red));
            holder.mTextViewTitle.setTextColor(mActivity.getResources()
                    .getColor(R.color.red));
            holder.mTextViewStatus.setText(R.string.lessThanThirtyDays);
            holder.mTextViewStatus.setTextColor(mActivity.getResources()
                    .getColor(R.color.red));
        } else if (daysDiff >= 31 && daysDiff <= 90) {
            // 90 Days Out
            holder.mTextViewProName.setTextColor(mActivity.getResources()
                    .getColor(R.color.yellow));
            holder.mTextViewExpirationDate.setTextColor(mActivity.getResources()
                    .getColor(R.color.yellow));
            holder.mTextViewTitle.setTextColor(mActivity.getResources()
                    .getColor(R.color.yellow));
            holder.mTextViewStatus.setText(R.string.thirtyOneToNinetyDays);
            holder.mTextViewStatus.setTextColor(mActivity.getResources()
                    .getColor(R.color.yellow));
        } else {
            // Over 90 Days out
            holder.mTextViewProName.setTextColor(mActivity.getResources()
                    .getColor(R.color.green));
            holder.mTextViewExpirationDate.setTextColor(mActivity.getResources()
                    .getColor(R.color.green));
            holder.mTextViewTitle.setTextColor(mActivity.getResources()
                    .getColor(R.color.green));
            holder.mTextViewStatus.setText(R.string.ninetyPlusDays);
            holder.mTextViewStatus.setTextColor(mActivity.getResources()
                    .getColor(R.color.green));
        }

        holder.mLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFragmentActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frameLayoutUserMain,
                                ProCredentialDetailFragment.newInstance(cred,
                                        true))
                        .addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mDataset != null) {
            return mDataset.size();
        }

        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextViewProName;
        private TextView mTextViewTitle;
        private TextView mTextViewExpirationDate;
        private TextView mTextViewStatus;
        private LinearLayout mLinearLayout;

        public ViewHolder(View _v) {
            super(_v);

            mTextViewProName = _v.findViewById(R.id.textViewAdminCredentialProName);
            mTextViewTitle = _v.findViewById(R.id.textViewAdminCredentialTitle);
            mTextViewExpirationDate = _v.findViewById(R.id.textViewAdminCredentialExpireDate);
            mTextViewStatus = _v.findViewById(R.id.textViewAdminCredentialStatus);
            mLinearLayout = _v.findViewById(R.id.linearLayoutAdminCredential);
        }
    }
}
