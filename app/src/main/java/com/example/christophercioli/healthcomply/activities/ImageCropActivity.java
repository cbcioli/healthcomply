package com.example.christophercioli.healthcomply.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.christophercioli.healthcomply.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOError;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import me.pqpo.smartcropperlib.view.CropImageView;

public class ImageCropActivity extends AppCompatActivity {

    // UI Elements
    private CropImageView mCropImageView;
    private Button mButtonSaveCroppedImage;
    private Uri mCapturedImageUri;

    // Constants
    private static final int CAMERA_IMAGE_REQUEST = 0x0010;
    public static final String EXTRA_CROPPED_IMAGE_PATH = "EXTRA_CROPPED_IMAGE_PATH";

    // Member Variables
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_crop);

        mCropImageView = findViewById(R.id.cropImageView);
        mButtonSaveCroppedImage = findViewById(R.id.buttonSaveCroppedImage);

        setButtonOnClickListeners(this);

        Intent getImageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (getImageIntent.resolveActivity(getPackageManager()) != null) {
            File imageFile = null;

            try {
                imageFile = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (imageFile != null) {
                mCapturedImageUri = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider", imageFile);
                getImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageUri);
                startActivityForResult(getImageIntent, CAMERA_IMAGE_REQUEST);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_IMAGE_REQUEST && resultCode == RESULT_OK) {
            try {
                Bitmap image = MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                        mCapturedImageUri);

                Matrix matrix = new Matrix();
                matrix.postRotate(90.0f);
                Bitmap rotatedImage = Bitmap.createBitmap(image, 0, 0, image.getWidth(),
                        image.getHeight(), matrix, true);

                mCropImageView.setImageToCrop(rotatedImage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (resultCode == RESULT_CANCELED) {
            finish();
        }
    }

    // Fragment Functionality
    private void setButtonOnClickListeners(final Activity _a) {
        mButtonSaveCroppedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgressDialog = new ProgressDialog(_a);
                mProgressDialog.setMessage("Saving Cropped Image...");
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mProgressDialog.show();
                new Thread() {
                    @Override
                    public void run() {
                        Bitmap croppedImage = mCropImageView.crop();

                        File oldFile = new File(mCapturedImageUri.getPath());
                        if (oldFile.exists()) {
                            oldFile.delete();
                        }

                        FileOutputStream fos;
                        String filePath = "";
                        try {
                            File croppedImageFile = createImageFile();
                            filePath = croppedImageFile.getAbsolutePath();
                            fos = new FileOutputStream(croppedImageFile);
                            croppedImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Intent resultIntent = new Intent();
                        resultIntent.putExtra(EXTRA_CROPPED_IMAGE_PATH, filePath);
                        setResult(RESULT_OK, resultIntent);
                        mProgressDialog.dismiss();
                        finish();
                    }
                }.start();
            }
        });
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US)
                .format(new Date());
        String imageFileName = "PNG_" + timeStamp;
        File storageDirectory = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".png", storageDirectory);

        return image;
    }
}
