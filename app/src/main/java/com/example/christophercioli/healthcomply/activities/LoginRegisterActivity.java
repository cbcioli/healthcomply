package com.example.christophercioli.healthcomply.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.example.christophercioli.healthcomply.R;
import com.example.christophercioli.healthcomply.fragments.LoginFragment;
import com.example.christophercioli.healthcomply.fragments.RegisterFragment;
import com.google.firebase.FirebaseApp;

public class LoginRegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_register);

        Intent inboundIntent = getIntent();
        String actionToTake = inboundIntent.getStringExtra(LaunchActivity.EXTRA_LOGIN_REGISTER);

        if (actionToTake.equals(LaunchActivity.LOGIN_VALUE)) {
            getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutLoginRegister,
                    LoginFragment.newInstance()).commit();
        } else if (actionToTake.equals(LaunchActivity.REGISTER_VALUE)) {
            getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutLoginRegister,
                    RegisterFragment.newInstance()).commit();
        }
    }
}
