// Christopher Cioli
// P&P6 - 1807
// AdminRosterAdapter.java

package com.example.christophercioli.healthcomply.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.christophercioli.healthcomply.R;
import com.example.christophercioli.healthcomply.contracts.DatabaseContracts;
import com.example.christophercioli.healthcomply.models.AdminCredential;
import com.example.christophercioli.healthcomply.models.Administrator;
import com.example.christophercioli.healthcomply.models.Professional;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AdminRosterAdapter extends RecyclerView.Adapter<AdminRosterAdapter.ViewHolder> {

    // Member Variables
    private Administrator mAdministrator;
    private ArrayList<Professional> mDataset;
    private Activity mActivity;
    private FragmentActivity mFragmentActivity;
    private DatabaseReference mDatabase;

    public AdminRosterAdapter(Administrator _admin, ArrayList<Professional> _professionals,
                              Activity _a, FragmentActivity _fragmentActivity) {
        mAdministrator = _admin;
        mDataset = _professionals;
        mActivity = _a;
        mFragmentActivity = _fragmentActivity;
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rosterCell = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_admin_roster_pro, parent, false);
        return new AdminRosterAdapter.ViewHolder(rosterCell);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Professional pro = mDataset.get(position);
        holder.mTextViewProName.setText(String.format("%s %s", pro.getFirstName(),
                pro.getLastName()));
        holder.mImageViewRemoveFromOrg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder b = new AlertDialog.Builder(mActivity);
                b.setTitle(String.format("Remove %s %s", pro.getFirstName(), pro.getLastName()))
                        .setMessage(String.format("Are you sure you'd like to remove %s %s from your organization? You will no longer have access to their credentials.",
                                pro.getFirstName(), pro.getLastName()))
                        .setNegativeButton("No", null)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mDataset.remove(pro);
                                notifyDataSetChanged();

                                mDatabase.child(DatabaseContracts.ORGANIZATIONS)
                                        .child(mAdministrator.getPrimaryAdminOrganizationId())
                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @SuppressWarnings("unchecked")
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                ArrayList<String> rosters = (ArrayList<String>)
                                                        dataSnapshot.child(DatabaseContracts.ROSTER_PROFESSIONALS)
                                                                .getValue();
                                                rosters.remove(pro.getId());
                                                mDatabase.child(DatabaseContracts.ORGANIZATIONS)
                                                        .child(mAdministrator.getPrimaryAdminOrganizationId())
                                                        .child(DatabaseContracts.ROSTER_PROFESSIONALS)
                                                        .setValue(rosters);
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                                // Nothing
                                            }
                                        });

                                pro.getOrganizationIds().remove(mAdministrator.getPrimaryAdminOrganizationId());
                                mDatabase.child(DatabaseContracts.USERS).child(pro.getId())
                                        .setValue(pro);
                            }
                        }).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mDataset != null) {
            return mDataset.size();
        }

        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextViewProName;
        private ImageView mImageViewRemoveFromOrg;

        public ViewHolder(View _v) {
            super(_v);

            mTextViewProName = _v.findViewById(R.id.textViewAdminRosterProName);
            mImageViewRemoveFromOrg = _v.findViewById(R.id.imageViewRemoveFromOrganization);
        }
    }
}
