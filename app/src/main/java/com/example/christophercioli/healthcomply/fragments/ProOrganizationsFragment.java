// Christopher Cioli
// P&P6 - 1807
// ProOrganizationsFragment.java

package com.example.christophercioli.healthcomply.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.christophercioli.healthcomply.R;
import com.example.christophercioli.healthcomply.activities.UserMainActivity;
import com.example.christophercioli.healthcomply.adapters.ProOrganizationAdapter;
import com.example.christophercioli.healthcomply.contracts.DatabaseContracts;
import com.example.christophercioli.healthcomply.models.Organization;
import com.example.christophercioli.healthcomply.models.Professional;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ProOrganizationsFragment extends Fragment {

    // UI Elements
    private RecyclerView mRecyclerViewOrganizations;
    private FloatingActionButton mButtonJoinOrganization;

    // Member Variables
    private ArrayList<Organization> mOrganizations;
    private RecyclerView.Adapter mRecyclerViewAdapter;
    private RecyclerView.LayoutManager mRecyclerViewLayoutManager;
    private FragmentActivity mFragmentActivity;
    private Professional mProfessional;
    private DatabaseReference mDatabase;

    // Constants
    private static final String ARG_PROFESSIONAL = "ARG_PROFESSIONAL";

    // Required Empty Constructor
    public ProOrganizationsFragment() { }

    public static ProOrganizationsFragment newInstance(Professional _pro) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_PROFESSIONAL, _pro);

        ProOrganizationsFragment fragment = new ProOrganizationsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mFragmentActivity = (FragmentActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pro_organizations, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() instanceof UserMainActivity) {
            UserMainActivity activity = (UserMainActivity) getActivity();
            activity.mCurrentSection = UserMainActivity.SECTION_PRO_ORGANIZATIONS;
            BottomNavigationView bottomNav = activity.findViewById(R.id.bottomNavUserMain);
            bottomNav.setSelectedItemId(R.id.buttonProNavOrganizations);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        if (getArguments() != null) {
            mProfessional = (Professional) getArguments().getSerializable(ARG_PROFESSIONAL);
        }

        if (getActivity() != null) {
            mRecyclerViewOrganizations = getActivity()
                    .findViewById(R.id.recyclerViewProOrganizations);
            mRecyclerViewOrganizations.setHasFixedSize(true);
            mRecyclerViewLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerViewOrganizations.setLayoutManager(mRecyclerViewLayoutManager);
            mButtonJoinOrganization = getActivity().findViewById(R.id.buttonJoinOrganization);

            setButtonOnClickListeners();
        }

        mOrganizations = new ArrayList<>();
        loadOrganizations();
    }

    // Fragment Functionality
    private void setButtonOnClickListeners() {
        mButtonJoinOrganization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText joinCodeEditText = new EditText(getContext());
                joinCodeEditText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Join an Organization")
                        .setMessage("Please enter your join code below")
                        .setView(joinCodeEditText)
                        .setNegativeButton("Cancel", null)
                        .setPositiveButton("Join", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                final String joinCode = joinCodeEditText.getText().toString().trim();
                                if (joinCode.isEmpty()) {
                                    joinCodeEditText.setError("Please enter a Join Code.");
                                } else {
                                    mDatabase.child(DatabaseContracts.JOIN_CODES)
                                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    boolean didFindJoinCode = false;
                                                    for (final DataSnapshot c : dataSnapshot.getChildren()) {
                                                        for (DataSnapshot d : c.getChildren()) {
                                                            if (d.getValue().equals(joinCode)) {
                                                                didFindJoinCode = true;

                                                                // Remove Join Code from DB
                                                                d.getRef().removeValue();

                                                                // Add Org to Professional's Organizations
                                                                if (mProfessional.getOrganizationIds() != null)
                                                                {
                                                                    mProfessional.getOrganizationIds().add(c.getKey());
                                                                } else {
                                                                    ArrayList<String> orgIds = new ArrayList<>();
                                                                    orgIds.add(c.getKey());
                                                                    mProfessional.setOrganizations(orgIds);
                                                                }

                                                                // Update Professional with new Org in DB
                                                                mDatabase.child(DatabaseContracts.USERS).child(mProfessional.getId()).setValue(mProfessional);

                                                                // Get New Org from Database and Add to Collection for RecyclerView
                                                                // Update Org roster with Pro in DB
                                                                mDatabase.child(DatabaseContracts.ORGANIZATIONS).child(c.getKey())
                                                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @SuppressWarnings("unchecked")
                                                                    @Override
                                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                        String id = c.getKey();
                                                                        String name = (String) dataSnapshot.child(DatabaseContracts.NAME).getValue();
                                                                        ArrayList<String> primaryAdmins = (ArrayList<String>)
                                                                                dataSnapshot.child(DatabaseContracts.PRIMARY_ADMINS).getValue();
                                                                        ArrayList<String> secondaryAdmins = (ArrayList<String>)
                                                                                dataSnapshot.child(DatabaseContracts.SECONDARY_ADMINS).getValue();
                                                                        ArrayList<String> roster = (ArrayList<String>)
                                                                                dataSnapshot.child(DatabaseContracts.ROSTER_PROFESSIONALS).getValue();

                                                                        Organization org = new Organization(id, name, primaryAdmins, secondaryAdmins, roster);
                                                                        if (org.getRosterProfessionalIds() != null) {
                                                                            org.getRosterProfessionalIds().add(mProfessional.getId());
                                                                        } else {
                                                                            ArrayList<String> newRoster = new ArrayList<>();
                                                                            newRoster.add(mProfessional.getId());
                                                                            org.setProfessionalsRoster(newRoster);
                                                                        }

                                                                        mOrganizations.add(org);
                                                                        mRecyclerViewAdapter.notifyDataSetChanged();

                                                                        mDatabase.child(DatabaseContracts.ORGANIZATIONS).child(org.getId()).setValue(org);

                                                                        Toast.makeText(getContext(), String.format("Join Code Approved - You have joined %s", org.getName()),
                                                                                Toast.LENGTH_LONG).show();
                                                                    }

                                                                    @Override
                                                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                                                        // Nothing
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }

                                                    if (!didFindJoinCode) {
                                                        AlertDialog.Builder b = new
                                                                AlertDialog.Builder(getContext());

                                                        b.setTitle("Failed to Join Organization")
                                                                .setMessage("The join code you provided did not belong to any active organization or has been used already. " +
                                                                        "Please check the join code and try again or contact the administrator who " +
                                                                        "sent you the join code.")
                                                                .setPositiveButton("OK", null)
                                                                .show();
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError
                                                                                databaseError) {
                                                    // Nothing
                                                }
                                            });
                                }
                            }
                        }).show();
            }
        });
    }

    private void loadOrganizations() {
        mDatabase.child(DatabaseContracts.ORGANIZATIONS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (mProfessional.getOrganizationIds() != null) {
                    for (DataSnapshot c : dataSnapshot.getChildren()) {
                        if (mProfessional.getOrganizationIds().contains(c.getKey())) {
                            String id = c.getKey();
                            String name = (String) c.child(DatabaseContracts.NAME).getValue();
                            ArrayList<String> primaryAdmins = (ArrayList<String>)
                                    c.child(DatabaseContracts.PRIMARY_ADMINS).getValue();
                            ArrayList<String> secondaryAdmins = (ArrayList<String>)
                                    c.child(DatabaseContracts.SECONDARY_ADMINS).getValue();
                            ArrayList<String> roster = (ArrayList<String>)
                                    c.child(DatabaseContracts.ROSTER_PROFESSIONALS).getValue();

                            Organization org = new Organization(id, name, primaryAdmins,
                                    secondaryAdmins, roster);
                            mOrganizations.add(org);
                        }
                    }
                }

                mRecyclerViewAdapter = new ProOrganizationAdapter(mOrganizations, getActivity(),
                        mFragmentActivity, mProfessional);
                mRecyclerViewOrganizations.setAdapter(mRecyclerViewAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Nothing
            }
        });
    }
}
