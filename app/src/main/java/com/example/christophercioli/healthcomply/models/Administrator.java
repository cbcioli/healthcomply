// Christopher Cioli
// P&P6 - 1807
// Administrator.java

package com.example.christophercioli.healthcomply.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Administrator extends User implements Serializable {
    private String primary_admin_org;
    private ArrayList<String> secondary_admin_orgs;

    public Administrator(String _id, String _firstName, String _lastName, String _title,
                         String _emailAddress, boolean _isAdmin,
                         String _primaryAdminOrganizationId,
                         ArrayList<String> _secondaryAdminOrganizationIds) {
        super(_id, _firstName, _lastName, _emailAddress, _title, _isAdmin);

        primary_admin_org = _primaryAdminOrganizationId;
        secondary_admin_orgs = _secondaryAdminOrganizationIds;
    }

    // Getters
    public String getPrimaryAdminOrganizationId() {
        return primary_admin_org;
    }

    public ArrayList<String> getSecondaryAdminOrganizationIds() {
        return secondary_admin_orgs;
    }

    // Setters
    public void setPrimaryAdminOrganizations(String _primaryAdminOrganization) {
        primary_admin_org = _primaryAdminOrganization;
    }

    public void setSecondaryAdminOrganizations(ArrayList<String> _secondaryAdminOrganizations) {
        secondary_admin_orgs = _secondaryAdminOrganizations;
    }
}
