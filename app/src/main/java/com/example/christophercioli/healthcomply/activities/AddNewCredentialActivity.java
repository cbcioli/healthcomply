// Christopher Cioli
// P&P6 - 1807
// AddNewCredentialActivity.java

package com.example.christophercioli.healthcomply.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.christophercioli.healthcomply.R;
import com.example.christophercioli.healthcomply.fragments.AddNewCredentialFragment;
import com.example.christophercioli.healthcomply.fragments.ProCredentialDetailFragment;
import com.example.christophercioli.healthcomply.models.Credential;

public class AddNewCredentialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_credential);

        boolean isEditing = false;
        Credential editCredential = null;
        if (getIntent().hasExtra(ProCredentialDetailFragment.EXTRA_EDIT_CREDENTIAL)) {
            editCredential = (Credential) getIntent()
                    .getSerializableExtra(ProCredentialDetailFragment.EXTRA_EDIT_CREDENTIAL);
            isEditing = true;

        }

        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutAddNewCredential,
                AddNewCredentialFragment.newInstance(isEditing, editCredential)).commit();
    }
}
