// Christopher Cioli
// P&P6 - 1807
// Credential.java

package com.example.christophercioli.healthcomply.models;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Credential implements Serializable, Comparable<Credential> {
    // Member Variables
    private String id;
    private String title;
    private String credential_id;
    private String certifying_body;
    private String expiration_date;
    private String image_id;

    public Credential(String _id, String _title, String _credentialId, String _certifyingBody,
                      String _expirationDate, String _image) {
        id = _id;
        title = _title;
        credential_id = _credentialId;
        certifying_body = _certifyingBody;
        expiration_date = _expirationDate;
        image_id = _image;
    }

    // Getters
    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getCredentialId() {
        return credential_id;
    }

    public String getCertifyingBody() {
        return certifying_body;
    }

    public String getExpirationDate() {
        return expiration_date;
    }

    public String getImage() {
        return image_id;
    }

    // Setters
    public void setId(String _id) {
        id = _id;
    }

    public void setTitle(String _title) {
        title = _title;
    }

    public void setCredentialId(String _credentialId) {
        credential_id = _credentialId;
    }
    public void setCertifyingBody(String _certifyingBody) {
        certifying_body = _certifyingBody;
    }

    public void setExpirationDate(String _expirationDate) {
        expiration_date = _expirationDate;
    }

    public void setImage(String _image) {
        image_id = _image;
    }

    // Class Functions
    public Date findDateTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy", Locale.US);
        try {
            return sdf.parse(expiration_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    // Comparable Methods
    @Override
    public int compareTo(@NonNull Credential credential) {
        return findDateTime().compareTo(credential.findDateTime());
    }
}
