// Christopher Cioli
// P&P6 - 1807
// AdminRosterFragment.java

package com.example.christophercioli.healthcomply.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.christophercioli.healthcomply.R;
import com.example.christophercioli.healthcomply.activities.UserMainActivity;
import com.example.christophercioli.healthcomply.adapters.AdminRosterAdapter;
import com.example.christophercioli.healthcomply.contracts.DatabaseContracts;
import com.example.christophercioli.healthcomply.models.Administrator;
import com.example.christophercioli.healthcomply.models.Organization;
import com.example.christophercioli.healthcomply.models.Professional;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AdminRosterFragment extends Fragment {

    // UI Elements
    private RecyclerView mRecyclerViewRoster;

    // Member Variables
    private Administrator mAdministrator;
    private ArrayList<Professional> mProfessionals;
    private DatabaseReference mDatabase;
    private RecyclerView.Adapter mRecyclerViewAdapter;
    private RecyclerView.LayoutManager mRecyclerViewLayoutManager;
    private FragmentActivity mFragmentActivity;

    // Constants
    private static final String ARG_ADMIN = "ARG_ADMIN";

    // Required Empty Constructor
    public AdminRosterFragment() { }

    public static AdminRosterFragment newInstance(Administrator _admin) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_ADMIN, _admin);

        AdminRosterFragment fragment = new AdminRosterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mFragmentActivity = (FragmentActivity) context;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() instanceof UserMainActivity) {
            UserMainActivity activity = (UserMainActivity) getActivity();
            activity.mCurrentSection = UserMainActivity.SECTION_ADMIN_ROSTER;
            BottomNavigationView bottomNav = activity.findViewById(R.id.bottomNavUserMain);
            bottomNav.setSelectedItemId(R.id.buttonAdminNavRoster);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_admin_roster, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        if (getArguments() != null) {
            mAdministrator = (Administrator) getArguments().getSerializable(ARG_ADMIN);
        }

        if (getActivity() != null) {
            mRecyclerViewRoster = getActivity().findViewById(R.id.recyclerViewAdminRoster);
            mRecyclerViewRoster.setHasFixedSize(true);
            mRecyclerViewLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerViewRoster.setLayoutManager(mRecyclerViewLayoutManager);
        }

        mProfessionals = new ArrayList<>();
        loadRosterProfessionals();

    }

    // Fragment Functionality
    private void loadRosterProfessionals() {
        mDatabase.child(DatabaseContracts.ORGANIZATIONS)
                .child(mAdministrator.getPrimaryAdminOrganizationId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @SuppressWarnings("unchecked")
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        final ArrayList<String> userIds = (ArrayList<String>) dataSnapshot
                                .child(DatabaseContracts.ROSTER_PROFESSIONALS).getValue();

                        mDatabase.child(DatabaseContracts.USERS).addListenerForSingleValueEvent(
                                new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                for (DataSnapshot c : dataSnapshot.getChildren()) {
                                    if (userIds != null && userIds.contains(c.getKey())) {
                                        String id = c.getKey();
                                        String firstName = (String)
                                                c.child(DatabaseContracts.FIRST_NAME).getValue();
                                        String lastName = (String)
                                                c.child(DatabaseContracts.LAST_NAME).getValue();
                                        String emailAddress = (String)
                                                c.child(DatabaseContracts.EMAIL_ADDRESS).getValue();
                                        String title = (String) c.child(DatabaseContracts.TITLE)
                                                        .getValue();
                                        boolean isAdmin = (boolean)
                                                c.child(DatabaseContracts.IS_ADMIN).getValue();
                                        ArrayList<String> credentialIds = (ArrayList<String>)
                                                c.child(DatabaseContracts.CREDENTIAL_IDS)
                                                        .getValue();
                                        ArrayList<String> organizationIds = (ArrayList<String>)
                                                c.child(DatabaseContracts.ORGANIZATION_IDS)
                                                        .getValue();

                                        Professional newPro = new Professional(id, firstName,
                                                lastName, emailAddress, title, isAdmin,
                                                credentialIds, organizationIds);
                                        mProfessionals.add(newPro);
                                    }
                                }

                                mRecyclerViewAdapter = new AdminRosterAdapter(mAdministrator,
                                        mProfessionals, getActivity(), mFragmentActivity);
                                mRecyclerViewRoster.setAdapter(mRecyclerViewAdapter);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                // Nothing
                            }
                        });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // Nothing
                    }
                });
    }
}
