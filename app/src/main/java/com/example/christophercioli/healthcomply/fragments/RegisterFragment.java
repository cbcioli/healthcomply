// Christopher Cioli
// P&P6 - 1807
// RegisterFragment.java

package com.example.christophercioli.healthcomply.fragments;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.christophercioli.healthcomply.R;
import com.example.christophercioli.healthcomply.activities.UserMainActivity;
import com.example.christophercioli.healthcomply.contracts.DatabaseContracts;
import com.example.christophercioli.healthcomply.models.Administrator;
import com.example.christophercioli.healthcomply.models.Professional;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.ArrayList;

public class RegisterFragment extends Fragment {

    // UI Elements
    private Button mButtonProfessional;
    private Button mButtonAdministrator;
    private Button mButtonRegister;
    private EditText mEditTextFirstName;
    private EditText mEditTextLastName;
    private EditText mEditTextEmailAddress;
    private EditText mEditTextTitle;
    private EditText mEditTextPassword;

    // Member Variables
    private FirebaseAuth mAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mDatabase;
    private String userType = "";

    // Constants
    private static final String admin = "ADMINISTRATOR";
    private static final String pro = "PROFESSIONAL";

    // Required Empty Constructor
    public RegisterFragment() { }

    public static RegisterFragment newInstance() {

        Bundle args = new Bundle();

        RegisterFragment fragment = new RegisterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() != null) {
            mButtonProfessional = getActivity().findViewById(R.id.buttonProfessional);
            mButtonAdministrator = getActivity().findViewById(R.id.buttonAdministrator);
            mButtonRegister = getActivity().findViewById(R.id.buttonRegisterNewUser);
            mEditTextFirstName = getActivity().findViewById(R.id.editTextRegisterFirstName);
            mEditTextLastName = getActivity().findViewById(R.id.editTextRegisterLastName);
            mEditTextTitle = getActivity().findViewById(R.id.editTextRegisterTitle);
            mEditTextEmailAddress = getActivity().findViewById(R.id.editTextRegisterEmailAddress);
            mEditTextPassword = getActivity().findViewById(R.id.editTextRegisterPassword);

            setOnClickListeners();
        }

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    // Fragment Functionality
    private void setUserType(String _newType) {
        userType = _newType;

        if (userType.equals(admin)) {
            mButtonAdministrator.setTextColor(getResources().getColor(R.color.white));
            mButtonAdministrator.setBackgroundTintList(ColorStateList
                    .valueOf(getResources().getColor(R.color.darkBlue)));

            mButtonProfessional.setTextColor(getResources().getColor(R.color.darkBlue));
            mButtonProfessional.setBackgroundTintList(ColorStateList
                    .valueOf(getResources().getColor(R.color.white)));
        } else {
            mButtonAdministrator.setTextColor(getResources().getColor(R.color.darkBlue));
            mButtonAdministrator.setBackgroundTintList(ColorStateList
                    .valueOf(getResources().getColor(R.color.white)));

            mButtonProfessional.setTextColor(getResources().getColor(R.color.white));
            mButtonProfessional.setBackgroundTintList(ColorStateList
                    .valueOf(getResources().getColor(R.color.darkBlue)));
        }
    }

    private void showEditTextError(EditText _e) {
        _e.setError(String.format("%s can't be left blank.", _e.getHint()));
    }

    private void setOnClickListeners() {
        mButtonProfessional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUserType(pro);
            }
        });

        mButtonAdministrator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUserType(admin);
            }
        });

        mButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isValid = true;

                ArrayList<EditText> fields = new ArrayList<>();
                fields.add(mEditTextFirstName);
                fields.add(mEditTextLastName);
                fields.add(mEditTextEmailAddress);
                fields.add(mEditTextTitle);
                fields.add(mEditTextPassword);

                for (EditText f : fields) {
                    if (f.getText().toString().isEmpty()) {
                        showEditTextError(f);
                        isValid = false;
                    }
                }

                if (userType.isEmpty()) {
                    Toast.makeText(getActivity(),
                            "Please choose your role - Professional or Administrator",
                            Toast.LENGTH_LONG).show();
                }

                if (!userType.isEmpty() && isValid) {
                    final String firstName = mEditTextFirstName.getText().toString();
                    final String lastName = mEditTextLastName.getText().toString();
                    final String emailAddress = mEditTextEmailAddress.getText().toString();
                    final String title = mEditTextTitle.getText().toString();
                    final String password = mEditTextPassword.getText().toString();

                    mAuth.createUserWithEmailAndPassword(emailAddress, password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        mFirebaseUser = mAuth.getCurrentUser();

                                        if (userType.equals(admin)) {
                                            Administrator newAdmin = new Administrator(
                                                    mFirebaseUser.getUid(), firstName, lastName,
                                                    title, emailAddress, true,
                                                    null,
                                                    null);

                                            mDatabase.child(DatabaseContracts.USERS)
                                                    .child(mFirebaseUser.getUid())
                                                    .setValue(newAdmin);

                                            Intent adminIntent = new Intent(getActivity(),
                                                    UserMainActivity.class);
                                            adminIntent.putExtra(
                                                    UserMainActivity.FRAGMENT_FUNCTION_KEY,
                                                    UserMainActivity.TO_ADMINISTRATOR_CREDENTIALS);
                                            adminIntent.putExtra(
                                                    UserMainActivity.EXTRA_ADMINISTRATOR_USER,
                                                    newAdmin);
                                            startActivity(adminIntent);
                                        } else if (userType.equals(pro)) {
                                            Professional newProfessional = new Professional(
                                                    mFirebaseUser.getUid(), firstName, lastName,
                                                    emailAddress, title, false,
                                                    null, null);

                                            mDatabase.child(DatabaseContracts.USERS)
                                                    .child(mFirebaseUser.getUid())
                                                    .setValue(newProfessional);

                                            Intent proIntent = new Intent(getActivity(),
                                                    UserMainActivity.class);
                                            proIntent.putExtra(
                                                    UserMainActivity.FRAGMENT_FUNCTION_KEY,
                                                    UserMainActivity.TO_PROFESSIONAL_CREDENTIALS);
                                            proIntent.putExtra(
                                                    UserMainActivity.EXTRA_PROFESSIONAL_USER,
                                                    newProfessional);
                                            startActivity(proIntent);
                                        }
                                    } else {
                                        Toast.makeText(getActivity(), "Unable to create user",
                                                Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                }
            }
        });
    }
}
