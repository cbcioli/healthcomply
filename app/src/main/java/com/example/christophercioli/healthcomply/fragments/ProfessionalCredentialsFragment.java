// Christopher Cioli
// P&P6 - 1807
// ProfessionalCredentialsFragment.java

package com.example.christophercioli.healthcomply.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import com.example.christophercioli.healthcomply.R;
import com.example.christophercioli.healthcomply.activities.AddNewCredentialActivity;
import com.example.christophercioli.healthcomply.activities.UserMainActivity;
import com.example.christophercioli.healthcomply.adapters.ProCredentialAdapter;
import com.example.christophercioli.healthcomply.adapters.ProOrganizationAdapter;
import com.example.christophercioli.healthcomply.contracts.DatabaseContracts;
import com.example.christophercioli.healthcomply.models.Credential;
import com.example.christophercioli.healthcomply.models.Professional;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

import static android.app.Activity.RESULT_OK;

public class ProfessionalCredentialsFragment extends Fragment {

    // UI Elements
    private RecyclerView mCredentialsRecyclerView;
    private ViewStub mEmptyStub;
    private FloatingActionButton mButtonNewCredential;

    // Member Variables
    private Professional mProfessional;
    private ArrayList<Credential> mCredentials;
    private DatabaseReference mDatabase;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private FragmentActivity mFragmentActivity;

    // Constants
    private static final String ARG_PROFESSIONAL = "ARG_PROFESSIONAL";
    public static final int NEW_CRED_REQ_CODE = 0x0111;

    // Required Empty Constructor
    public ProfessionalCredentialsFragment() { }

    public static ProfessionalCredentialsFragment newInstance(Professional _pro) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_PROFESSIONAL, _pro);

        ProfessionalCredentialsFragment fragment = new ProfessionalCredentialsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_professional_credentials, container,
                false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mFragmentActivity = (FragmentActivity) context;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() instanceof UserMainActivity) {
            UserMainActivity activity = (UserMainActivity) getActivity();
            activity.mCurrentSection = UserMainActivity.SECTION_PRO_CREDENTIALS;
            BottomNavigationView bottomNav = activity.findViewById(R.id.bottomNavUserMain);
            bottomNav.setSelectedItemId(R.id.buttonProNavCredentials);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments() != null) {
            mProfessional = (Professional) getArguments().getSerializable(ARG_PROFESSIONAL);
        }

        if (getActivity() != null) {
            mEmptyStub = getActivity().findViewById(android.R.id.empty);
            mButtonNewCredential = getActivity().findViewById(R.id.buttonAddNewCredential);
            mCredentialsRecyclerView = getActivity().findViewById(android.R.id.list);
            mCredentialsRecyclerView.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(getActivity());
            mCredentialsRecyclerView.setLayoutManager(mLayoutManager);

            setButtonOnClickListeners();
        }

        mCredentials = new ArrayList<>();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        loadCredentials();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_CRED_REQ_CODE && resultCode == RESULT_OK) {
            Credential newCredential = (Credential)
                    data.getSerializableExtra(AddNewCredentialFragment.EXTRA_NEW_CREDENTIAL);
            mCredentials.add(newCredential);
            Collections.sort(mCredentials);
            if (mProfessional.getCredentialIds() != null) {
                mProfessional.getCredentialIds().add(newCredential.getId());
            } else {
                ArrayList<String> creds = new ArrayList<>();
                creds.add(newCredential.getId());
                mProfessional.setCredentials(creds);
            }
            mAdapter.notifyDataSetChanged();

            mDatabase.child(DatabaseContracts.USERS).child(mProfessional.getId())
                    .setValue(mProfessional);
        }
    }

    // Fragment Functionality
    private void loadCredentials() {
        if (mProfessional.getCredentialIds() == null
                || mProfessional.getCredentialIds().size() == 0) {
            mCredentialsRecyclerView.setVisibility(View.GONE);
            mEmptyStub.setVisibility(View.VISIBLE);
            mAdapter = new ProCredentialAdapter(mCredentials, getActivity(), mFragmentActivity);
            mCredentialsRecyclerView.setAdapter(mAdapter);
        } else {

            mDatabase.child(DatabaseContracts.CREDENTIALS).addListenerForSingleValueEvent(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (DataSnapshot c : dataSnapshot.getChildren()) {
                                if (mProfessional.getCredentialIds().contains(c.getKey())) {
                                    String id = c.getKey();
                                    String title = (String) c.child(DatabaseContracts.TITLE)
                                            .getValue();
                                    String credentialId = (String) c.child(DatabaseContracts
                                            .CREDENTIAL_ID)
                                            .getValue();
                                    String certifyingBody = (String) c.child(DatabaseContracts
                                            .CERTIFYING_BODY)
                                            .getValue();
                                    String expirationDate = (String) c.child(DatabaseContracts
                                            .EXPIRATION_DATE)
                                            .getValue();
                                    String imageId = (String) c.child(DatabaseContracts.IMAGE_ID)
                                            .getValue();

                                    Credential newCredential = new Credential(id, title,
                                            credentialId, certifyingBody, expirationDate, imageId);
                                    mCredentials.add(newCredential);
                                }
                            }

                            Collections.sort(mCredentials);
                            mAdapter = new ProCredentialAdapter(mCredentials, getActivity(),
                                    mFragmentActivity);
                            mCredentialsRecyclerView.setAdapter(mAdapter);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            // Nothing
                        }
                    });
        }
    }

    private void setButtonOnClickListeners() {
        mButtonNewCredential.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newCredentialIntent = new Intent(getActivity(),
                        AddNewCredentialActivity.class);
                startActivityForResult(newCredentialIntent, NEW_CRED_REQ_CODE);
            }
        });
    }
}
