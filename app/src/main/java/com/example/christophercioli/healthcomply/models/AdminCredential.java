// Christopher Cioli
// P&P6 -1807
// AdminCredential.java

package com.example.christophercioli.healthcomply.models;

public class AdminCredential extends Credential {
    // Member Variables
    private String mProId;
    private String mProFullName;

    public AdminCredential(String _id, String _title, String _credentialId, String _certifyingBody,
                           String _expirationDate, String _image, String _proId,
                           String _proFullName) {
        super(_id, _title, _credentialId, _certifyingBody, _expirationDate, _image);
        mProId = _proId;
        mProFullName = _proFullName;
    }

    // Getters
    public String getProId() {
        return mProId;
    }

    public String getProFullName() {
        return mProFullName;
    }
}
