// Christopher Cioli
// P&P6 - 1807
// LoginFragment.java

package com.example.christophercioli.healthcomply.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.christophercioli.healthcomply.R;
import com.example.christophercioli.healthcomply.activities.UserMainActivity;
import com.example.christophercioli.healthcomply.contracts.DatabaseContracts;
import com.example.christophercioli.healthcomply.models.Administrator;
import com.example.christophercioli.healthcomply.models.Professional;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;

public class LoginFragment extends Fragment {

    // UI Elements
    private EditText mEditTextEmailAddress;
    private EditText mEditTextPassword;
    private Button mButtonLogin;

    // Member Variables
    private FirebaseAuth mAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mDatabase;

    // Required Empty Constructor
    public LoginFragment() { }

    public static LoginFragment newInstance() {

        Bundle args = new Bundle();

        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() != null) {
            mEditTextEmailAddress = getActivity().findViewById(R.id.editTextEmailAddress);
            mEditTextPassword = getActivity().findViewById(R.id.editTextPassword);
            mButtonLogin = getActivity().findViewById(R.id.buttonUserLogin);
            setButtonOnClickListener();
        }

        mAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    // Fragment Functionality
    private void showUserLandingScreen() {
        if (mFirebaseUser != null) {
            final String userId = mFirebaseUser.getUid();

            mDatabase.child(DatabaseContracts.USERS).child(userId).addListenerForSingleValueEvent(
                    new ValueEventListener() {
                        @SuppressWarnings("unchecked")
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            boolean isAdmin = (boolean) dataSnapshot
                                    .child(DatabaseContracts.IS_ADMIN).getValue();
                            String firstName = (String) dataSnapshot.
                                    child(DatabaseContracts.FIRST_NAME).getValue();
                            String lastName = (String) dataSnapshot
                                    .child(DatabaseContracts.LAST_NAME).getValue();
                            String emailAddress = (String) dataSnapshot
                                    .child(DatabaseContracts.EMAIL_ADDRESS).getValue();
                            String title = (String) dataSnapshot.child(DatabaseContracts.TITLE)
                                    .getValue();

                            if (isAdmin) {
                                String primaryAdminOrganizationIds = (String)
                                        dataSnapshot.child(DatabaseContracts.PRIMARY_ADMIN_ORG)
                                        .getValue();
                                ArrayList<String> secondaryAdminOrganizationIds = (ArrayList<String>)
                                        dataSnapshot.child(DatabaseContracts.SECONDARY_ADMIN_ORGS)
                                        .getValue();

                                Administrator admin = new Administrator(userId, firstName, lastName,
                                        title, emailAddress, isAdmin, primaryAdminOrganizationIds,
                                        secondaryAdminOrganizationIds);

                                Intent adminIntent = new Intent(getActivity(),
                                        UserMainActivity.class);
                                adminIntent.putExtra(UserMainActivity.FRAGMENT_FUNCTION_KEY,
                                        UserMainActivity.TO_ADMINISTRATOR_CREDENTIALS);
                                adminIntent.putExtra(UserMainActivity.EXTRA_ADMINISTRATOR_USER, admin);
                                startActivity(adminIntent);
                            } else {
                                ArrayList<String> credentialIds = (ArrayList<String>)
                                        dataSnapshot.child(DatabaseContracts.CREDENTIAL_IDS)
                                                .getValue();
                                ArrayList<String> organizationIds = (ArrayList<String>)
                                        dataSnapshot.child(DatabaseContracts.ORGANIZATION_IDS)
                                                .getValue();

                                Professional pro = new Professional(userId, firstName, lastName,
                                        title, emailAddress, isAdmin, credentialIds,
                                        organizationIds);

                                Intent proIntent = new Intent(getActivity(),
                                        UserMainActivity.class);
                                proIntent.putExtra(UserMainActivity.FRAGMENT_FUNCTION_KEY,
                                        UserMainActivity.TO_PROFESSIONAL_CREDENTIALS);
                                proIntent.putExtra(UserMainActivity.EXTRA_PROFESSIONAL_USER, pro);
                                startActivity(proIntent);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            // Nothing
                        }
                    }
            );
        }
    }

    private void setButtonOnClickListener() {
        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailAddress = mEditTextEmailAddress.getText().toString();
                String password = mEditTextPassword.getText().toString();

                if (emailAddress.isEmpty()) {
                    mEditTextEmailAddress.setError("Email Address must not be left blank");
                }

                if (password.isEmpty()) {
                    mEditTextPassword.setError("Password must not be left blank");
                }

                if (!emailAddress.isEmpty() && !password.isEmpty()) {
                    mAuth.signInWithEmailAndPassword(emailAddress, password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        mFirebaseUser = mAuth.getCurrentUser();
                                        showUserLandingScreen();
                                    } else {
                                        Toast.makeText(getActivity(),
                                                "Error: Unable to sign in with" +
                                                        "provided email address and password.",
                                                Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                }
            }
        });
    }
}
