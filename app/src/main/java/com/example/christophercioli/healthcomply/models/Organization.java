// Christopher Cioli
// P&P6 - 1807
// Organization.java

package com.example.christophercioli.healthcomply.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Organization implements Serializable {
    // Member Variables
    private String id;
    private String name;
    private ArrayList<String> primary_admins;
    private ArrayList<String> secondary_admins;
    private ArrayList<String> professionals;

    public Organization(String _id, String _name, ArrayList<String> _primaryAdministrators,
                         ArrayList<String> _secondaryAdministrators,
                         ArrayList<String> _professionalRoster) {
        id = _id;
        name = _name;
        primary_admins = _primaryAdministrators;
        secondary_admins = _secondaryAdministrators;
        professionals = _professionalRoster;
    }

    // Getters
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ArrayList<String> getPrimaryAdministratorIds() {
        return primary_admins;
    }

    public ArrayList<String> getSecondaryAdministratorIds() {
        return secondary_admins;
    }

    public ArrayList<String> getRosterProfessionalIds() {
        return professionals;
    }

    // Setters
    public void setId(String _id) {
        id = _id;
    }

    public void setName(String _name) {
        name = _name;
    }
    public void setPrimaryAdministrators(ArrayList<String> _primaryAdministrators) {
        primary_admins = _primaryAdministrators;
    }

    public void setSecondaryAdministrators(ArrayList<String> _secondaryAdministrators) {
        secondary_admins = _secondaryAdministrators;
    }

    public void setProfessionalsRoster(ArrayList<String> _professionalRoster) {
        professionals = _professionalRoster;
    }
}
