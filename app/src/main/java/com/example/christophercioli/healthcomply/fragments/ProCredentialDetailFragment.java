// Christopher Cioli
// P&P6 - 1807
// ProCredentialDetailFragment.java

package com.example.christophercioli.healthcomply.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.load.resource.transcode.BitmapToGlideDrawableTranscoder;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.example.christophercioli.healthcomply.R;
import com.example.christophercioli.healthcomply.activities.AddNewCredentialActivity;
import com.example.christophercioli.healthcomply.contracts.DatabaseContracts;
import com.example.christophercioli.healthcomply.models.Credential;
import com.example.christophercioli.healthcomply.models.Professional;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.auth.api.signin.internal.Storage;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;

public class ProCredentialDetailFragment extends Fragment {

    // UI Elements
    private TextView mTextViewTitle;
    private TextView mTextViewCredentialId;
    private TextView mTextViewCertifyingBody;
    private TextView mTextViewExpirationDate;
    private ImageView mImageViewCredentialImage;
    private Button mButtonEdit;
    private Button mButtonShare;

    // Constants
    private static final String CREDENTIAL_ARG = "CREDENTIAL_ARG";
    private static final String IS_ADMIN_ARG = "IS_ADMIN_ARG";
    public static final String EXTRA_EDIT_CREDENTIAL = "EXTRA_EDIT_CREDENTIAL";
    public static final int EDIT_REQUEST_CODE = 14253;

    // Member Variables
    private boolean mIsAdminView;
    private Credential mCredential;
    private StorageReference mStorage;
    private File mTempSharedFile;
    private GlideDrawable mGlideDrawable;
    private DatabaseReference mDatabase;

    // Required Empty Constructor
    public ProCredentialDetailFragment() { }

    public static ProCredentialDetailFragment newInstance(Credential _cred, boolean _isAdminView) {

        Bundle args = new Bundle();
        args.putSerializable(CREDENTIAL_ARG, _cred);
        args.putBoolean(IS_ADMIN_ARG, _isAdminView);

        ProCredentialDetailFragment fragment = new ProCredentialDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pro_credential_detail, container,
                false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mStorage = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        if (getActivity() != null && getArguments() != null) {
            mCredential = (Credential) getArguments().getSerializable(CREDENTIAL_ARG);
            mIsAdminView = getArguments().getBoolean(IS_ADMIN_ARG);

            mTextViewTitle = getActivity().findViewById(R.id.textViewCredentialDetailTitle);
            mTextViewCredentialId = getActivity()
                    .findViewById(R.id.textViewCredentialDetailCredentialId);
            mTextViewCertifyingBody = getActivity()
                    .findViewById(R.id.textViewCredentialDetailCertifyingBody);
            mTextViewExpirationDate = getActivity()
                    .findViewById(R.id.textViewCredentialDetailExpirationDate);
            mImageViewCredentialImage = getActivity()
                    .findViewById(R.id.imageViewCredentialDetailImage);
            mButtonEdit = getActivity().findViewById(R.id.buttonEditCredential);
            mButtonShare = getActivity().findViewById(R.id.buttonShareCredential);

            if (mIsAdminView) {
                mButtonShare.setVisibility(View.GONE);
                mButtonEdit.setVisibility(View.GONE);
            }

            setButtonOnClickListeners();
            loadCredentialDetails();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == EDIT_REQUEST_CODE && resultCode == RESULT_OK) {
            mCredential = (Credential) data
                    .getSerializableExtra(AddNewCredentialFragment.EXTRA_EDITED_CREDENTIAL);
            loadCredentialDetails();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        deleteTempFile(mTempSharedFile);
    }

    // Fragment Functionality
    private void loadCredentialDetails() {
        if (mCredential.getImage() != null) {
            GlideDrawableImageViewTarget target =
                    new GlideDrawableImageViewTarget(mImageViewCredentialImage);
            Glide.with(getContext()).using(new FirebaseImageLoader())
                    .load(mStorage.child("user-images").child(mCredential.getImage()))
                    .thumbnail(Glide.with(getContext()).load(R.drawable.blocks_1s_72px))
                    .listener(new RequestListener<StorageReference, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, StorageReference model,
                                                   Target<GlideDrawable> target,
                                                   boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource,
                                                       StorageReference model,
                                                       Target<GlideDrawable> target,
                                                       boolean isFromMemoryCache,
                                                       boolean isFirstResource) {
                            mGlideDrawable = resource;
                            return false;
                        }
                    })
                    .into(target);
        }

        mTextViewTitle.setText(mCredential.getTitle());
        if (mCredential.getCredentialId() == null || mCredential.getCredentialId().isEmpty()) {
            mTextViewCredentialId.setTypeface(mTextViewCredentialId.getTypeface(), Typeface.ITALIC);
            mTextViewCredentialId.setText(R.string.noIdProvided);
        } else {
            mTextViewCredentialId.setText(mCredential.getCredentialId());
        }
        mTextViewCertifyingBody.setText(mCredential.getCertifyingBody());
        mTextViewExpirationDate.setText(mCredential.getExpirationDate());
    }

    private void setButtonOnClickListeners() {
        mButtonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent editIntent = new Intent(getActivity(), AddNewCredentialActivity.class);
                editIntent.putExtra(EXTRA_EDIT_CREDENTIAL, mCredential);
                startActivityForResult(editIntent, EDIT_REQUEST_CODE);
            }
        });

        mButtonShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, String.format("%s Credential Info",
                        mCredential.getTitle()));

                String bodyString = "";
                bodyString += String.format("Title: %s\n", mCredential.getTitle());
                bodyString += String.format("Certifying Body: %s\n",
                        mCredential.getCertifyingBody());
                if (mCredential.getCredentialId() != null &&
                        !mCredential.getCredentialId().isEmpty()) {
                    bodyString += String.format("Credential ID: %s\n",
                            mCredential.getCredentialId());
                } else {
                    bodyString += "Credential ID: - No ID Provided -\n";
                }
                bodyString += String.format("Expiration Date: %s", mCredential.getExpirationDate());
                bodyString += "\n\nThis message was sent from the Health Comply app";

                shareIntent.putExtra(Intent.EXTRA_TEXT, bodyString);

                // Image work
                if (mCredential.getImage() != null || !mCredential.getImage().isEmpty()) {
                    Bitmap bmp = ((GlideBitmapDrawable)mGlideDrawable.getCurrent()).getBitmap();
                    mTempSharedFile = saveImageLocalPrivate(bmp);
                    shareIntent.putExtra(Intent.EXTRA_STREAM,
                            FileProvider.getUriForFile(getContext(),
                                    "com.example.android.fileprovider", mTempSharedFile));
                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    shareIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }
                
                startActivity(Intent.createChooser(shareIntent, "Share Credential To..."));
            }
        });
    }

    private File saveImageLocalPrivate(Bitmap _bmp) {
        File directory = getContext().getExternalFilesDir("Pictures");
        File storedFile = new File(directory,"credentialPicture.jpg");
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(storedFile);
            _bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return storedFile;
    }

    private void deleteTempFile(File _tempFile) {
        if (_tempFile != null && _tempFile.exists()) {
            _tempFile.delete();
        }
    }
}
