// Christopher Cioli
// P&P6 - 1807
// LaunchActivity.java

package com.example.christophercioli.healthcomply.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.christophercioli.healthcomply.R;
import com.example.christophercioli.healthcomply.contracts.DatabaseContracts;
import com.example.christophercioli.healthcomply.models.Administrator;
import com.example.christophercioli.healthcomply.models.Professional;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class LaunchActivity extends AppCompatActivity {

    // UI Elements
    private Button mButtonLogin;
    private Button mButtonRegister;

    // Member Variables
    private FirebaseAuth mAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mDatabase;
    private ProgressDialog mProgressDialog;

    // Key Constants
    public static String EXTRA_LOGIN_REGISTER = "KEY_LOGIN_REGISTER";
    public static String LOGIN_VALUE = "LOGIN";
    public static String REGISTER_VALUE = "REGISTER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Logging You In...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        if (mFirebaseUser != null) {
            mProgressDialog.show();
            loadUser(this);
        }

        mButtonLogin = findViewById(R.id.buttonLogin);
        mButtonRegister = findViewById(R.id.buttonRegister);

        setOnClickListeners();
    }

    // Activity Functionality
    private void setOnClickListeners() {
        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Go To Login
                Intent loginIntent = new Intent(LaunchActivity.this,
                        LoginRegisterActivity.class);
                loginIntent.putExtra(EXTRA_LOGIN_REGISTER, LOGIN_VALUE);
                startActivity(loginIntent);
            }
        });

        mButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Go To Register
                Intent registerIntent = new Intent(LaunchActivity.this,
                        LoginRegisterActivity.class);
                registerIntent.putExtra(EXTRA_LOGIN_REGISTER, REGISTER_VALUE);
                startActivity(registerIntent);
            }
        });
    }

    private void loadUser(final Activity a) {
        final String userId = mFirebaseUser.getUid();
        mDatabase.child(DatabaseContracts.USERS).child(userId)
                .addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @SuppressWarnings("unchecked")
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        boolean isAdmin = (boolean) dataSnapshot
                                .child(DatabaseContracts.IS_ADMIN).getValue();
                        String firstName = (String) dataSnapshot.
                                child(DatabaseContracts.FIRST_NAME).getValue();
                        String lastName = (String) dataSnapshot
                                .child(DatabaseContracts.LAST_NAME).getValue();
                        String emailAddress = (String) dataSnapshot
                                .child(DatabaseContracts.EMAIL_ADDRESS).getValue();
                        String title = (String) dataSnapshot.child(DatabaseContracts.TITLE)
                                .getValue();

                        if (isAdmin) {
                            String primaryAdminOrganizationIds = (String)
                                    dataSnapshot.child(DatabaseContracts.PRIMARY_ADMIN_ORG)
                                            .getValue();
                            ArrayList<String> secondaryAdminOrganizationIds = (ArrayList<String>)
                                    dataSnapshot.child(DatabaseContracts.SECONDARY_ADMIN_ORGS)
                                            .getValue();

                            Administrator admin = new Administrator(userId, firstName, lastName,
                                    title, emailAddress, isAdmin, primaryAdminOrganizationIds,
                                    secondaryAdminOrganizationIds);

                            Intent adminIntent = new Intent(a,
                                    UserMainActivity.class);
                            adminIntent.putExtra(UserMainActivity.FRAGMENT_FUNCTION_KEY,
                                    UserMainActivity.TO_ADMINISTRATOR_CREDENTIALS);
                            adminIntent.putExtra(UserMainActivity.EXTRA_ADMINISTRATOR_USER, admin);
                            startActivity(adminIntent);
                            mProgressDialog.dismiss();
                        } else {
                            ArrayList<String> credentialIds = (ArrayList<String>)
                                    dataSnapshot.child(DatabaseContracts.CREDENTIAL_IDS)
                                            .getValue();
                            ArrayList<String> organizationIds = (ArrayList<String>)
                                    dataSnapshot.child(DatabaseContracts.ORGANIZATION_IDS)
                                            .getValue();

                            Professional pro = new Professional(userId, firstName, lastName,
                                    emailAddress, title, isAdmin, credentialIds,
                                    organizationIds);

                            Intent proIntent = new Intent(a,
                                    UserMainActivity.class);
                            proIntent.putExtra(UserMainActivity.FRAGMENT_FUNCTION_KEY,
                                    UserMainActivity.TO_PROFESSIONAL_CREDENTIALS);
                            proIntent.putExtra(UserMainActivity.EXTRA_PROFESSIONAL_USER, pro);
                            startActivity(proIntent);
                            mProgressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                }
        );
    }
}
