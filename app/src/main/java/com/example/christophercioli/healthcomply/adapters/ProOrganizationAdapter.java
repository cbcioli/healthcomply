// Christopher Cioli
// P&P6 - 1807
// ProOrganizationAdapter.java

package com.example.christophercioli.healthcomply.adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.christophercioli.healthcomply.R;
import com.example.christophercioli.healthcomply.contracts.DatabaseContracts;
import com.example.christophercioli.healthcomply.models.Organization;
import com.example.christophercioli.healthcomply.models.Professional;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ProOrganizationAdapter extends RecyclerView.Adapter<ProOrganizationAdapter.ViewHolder> {
    private ArrayList<Organization> mDataset;
    private Professional mProfessional;
    private Activity mActivity;
    private FragmentActivity mFragmentActivity;
    private DatabaseReference mDatabase;

    public ProOrganizationAdapter(ArrayList<Organization> _orgs, Activity _a,
                                  FragmentActivity _fragmentActivity, Professional _pro) {
        mDataset = _orgs;
        mProfessional = _pro;
        mActivity = _a;
        mFragmentActivity = _fragmentActivity;
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    @NonNull
    @Override
    public ProOrganizationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                                int viewType) {
        View organizationCell = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_pro_organization, parent, false);

        return new ProOrganizationAdapter.ViewHolder(organizationCell);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Organization org = mDataset.get(position);
        holder.mTextViewOrgName.setText(org.getName());
        holder.mImageViewLeaveOrg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                builder.setTitle("Leave Organization")
                        .setMessage("Are you sure you want to leave " + org.getName() +
                        "? This organization will no longer have access to your credentials.")
                        .setNegativeButton("Cancel", null)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mDataset.remove(org);
                                mProfessional.getOrganizationIds().remove(org.getId());
                                notifyDataSetChanged();
                                Toast.makeText(mActivity, "You've been removed from " +
                                        org.getName(), Toast.LENGTH_LONG).show();

                                mDatabase.child(DatabaseContracts.USERS)
                                        .child(mProfessional.getId())
                                        .child(DatabaseContracts.ORGANIZATION_IDS)
                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot
                                                                             dataSnapshot) {
                                                for (DataSnapshot c : dataSnapshot.getChildren()) {
                                                    if (c.getValue().equals(org.getId())) {
                                                        c.getRef().removeValue();
                                                        break;
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError
                                                                            databaseError) {
                                                // Nothing
                                            }
                                        });

                                mDatabase.child(DatabaseContracts.ORGANIZATIONS).child(org.getId())
                                        .child(DatabaseContracts.ROSTER_PROFESSIONALS)
                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot
                                                                             dataSnapshot) {
                                                for (DataSnapshot c : dataSnapshot.getChildren()) {
                                                    if (c.getValue().equals(mProfessional.getId()))
                                                    {
                                                        c.getRef().removeValue();
                                                        break;
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError
                                                                            databaseError) {
                                                // Nothing
                                            }
                                        });
                            }
                        }).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mDataset != null) {
            return mDataset.size();
        }

        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextViewOrgName;
        private ImageView mImageViewLeaveOrg;

        public ViewHolder(View _v) {
            super(_v);

            mTextViewOrgName = _v.findViewById(R.id.textViewProOrganizationName);
            mImageViewLeaveOrg = _v.findViewById(R.id.imageViewLeaveOrganization);
        }
    }
}
